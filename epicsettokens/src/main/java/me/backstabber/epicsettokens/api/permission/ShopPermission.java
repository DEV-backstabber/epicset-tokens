package me.backstabber.epicsettokens.api.permission;


import org.bukkit.entity.Player;


public interface ShopPermission {
	/**
	 * Checks if a player has this permission
	 * @param player to check
	 * @return true if player has permission
	 */
	public boolean hasPermission(Player player);
	/**
	 * Sends the no permission message to the player
	 * @param player to send message
	 */
	public void sendFailMessage(Player player);
	/**
	 * Check permission for the player & sends no permission
	 * message as well
	 * @param player to check
	 * @return true if player has permission
	 */
	public boolean checkPermission(Player player);
}
