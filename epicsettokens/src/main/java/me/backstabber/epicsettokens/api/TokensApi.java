package me.backstabber.epicsettokens.api;

import java.util.List;

import org.bukkit.entity.Player;

import me.backstabber.epicsettokens.api.data.PlayerData;
import me.backstabber.epicsettokens.api.managers.ShopManager;
import me.backstabber.epicsettokens.api.managers.TokensManager;
/**
 * This is the api instance of this plugin
 * Since the plugin has two parts
 * tokens & tokenshop only two managers are available
 * @author Backstabber
 *
 */
import me.backstabber.epicsettokens.api.permission.ShopPermission;
public interface TokensApi {
	/**
	 * Fetch an instance of the TokensManager used
	 * for storing & updating player's tokens
	 * @return MySqlManager instance
	 */
	public TokensManager getTokensManager();
	/**
	 * Fetch shopmanager instance used to store
	 * open & close tokenshops for players
	 * @return ShopManager instance
	 */
	public ShopManager getShopManager();
	/**
	 * Fetch a player's data containing information about his purchases
	 * @param player
	 * @return
	 */
	public PlayerData getPlayerData(Player player);
	/**
	 * Create a ShopPermission for a tokenshop
	 * @param permission node for this object
	 * @param message for when player doesnt have permission
	 * @return shop permission useable in builder
	 */
	public ShopPermission createPermission(String permission,List<String> message);
	/**
	 * Create a ShopPermission for a tokenshop
	 * @param permission node for this object
	 * @param message for when player doesnt have permission
	 * @return shop permission useable in builder
	 */
	public ShopPermission createPermission(String permission,String message);
	
}
