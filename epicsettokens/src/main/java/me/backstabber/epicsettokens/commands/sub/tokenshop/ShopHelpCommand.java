package me.backstabber.epicsettokens.commands.sub.tokenshop;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.commands.SubShopCommands;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.JSONMessage;


public class ShopHelpCommand extends SubShopCommands {
	private String name = "help";

	public ShopHelpCommand(EpicSetTokens plugin) {
		super(plugin);
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender))
			return;
		onCommandByConsole(sender, sub);
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		if(sub.length<=1||sub[1].equalsIgnoreCase("1")) {
			sender.sendMessage(ColorUtils.applyColor("&e&m--------------&6[&e&lEpicset-Tokens&7(1/4)&6]&e&m---------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop  &7Open main shop"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop <shop_name>  &7Open shop by name."));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop create <shop_name>  &7Create a new shop."));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop delete <shop_name>  &7Delete a shop."));
			sender.sendMessage(ColorUtils.applyColor(" "));
			if(sender instanceof Player) {
				JSONMessage.create(ColorUtils.applyColor("&e&m---------------&6<<<<<<"))
				.then(ColorUtils.applyColor("&e&m-----&7|&e1&7|&e&m-----")).tooltip(ColorUtils.applyColor("&fPage 1 of 4."))
				.then(ColorUtils.applyColor("&6>>>>>>&e&m--------------")).tooltip(ColorUtils.applyColor("&fClick for next page.")).runCommand("/tokenshop help 2").send((Player)sender);
			
			}
		}
		else if(sub[1].equalsIgnoreCase("2")) {
			sender.sendMessage(ColorUtils.applyColor("&e&m--------------&6[&e&lEpicset-Tokens&7(2/4)&6]&e&m---------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop setsize <shop_name> <size>  &7Set size of a shop."));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop setname <shop_name> <display name>  &7Set display name of shop."));
			sender.sendMessage(ColorUtils.applyColor("      &7Set display name of shop."));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop setbackground <shop_name>"));
			sender.sendMessage(ColorUtils.applyColor("      &7Set item in hand as background."));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop addpermission"));
			sender.sendMessage(ColorUtils.applyColor("      &f<shop_name> <permission> <message>"));
			sender.sendMessage(ColorUtils.applyColor("            &7Add a permission to a shop."));
			if(sender instanceof Player) {
				JSONMessage.create(ColorUtils.applyColor("&e&m---------------&6<<<<<<")).tooltip(ColorUtils.applyColor("&fClick for previous page.")).runCommand("/tokenshop help 1")
				.then(ColorUtils.applyColor("&e&m-----&7|&e2&7|&e&m-----")).tooltip(ColorUtils.applyColor("&fPage 2 of 4."))
				.then(ColorUtils.applyColor("&6>>>>>>&e&m--------------")).tooltip(ColorUtils.applyColor("&fClick for next page.")).runCommand("/tokenshop help 3").send((Player)sender);
			
			}
		}
		else if(sub[1].equalsIgnoreCase("3")) {
			sender.sendMessage(ColorUtils.applyColor("&e&m--------------&6[&e&lEpicset-Tokens&7(3/4)&6]&e&m---------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop add <shop_name> <slot> <trigger>"));
			sender.sendMessage(ColorUtils.applyColor("      &7Add non purchasable items in shop."));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop remove <shop_name> <slot>"));
			sender.sendMessage(ColorUtils.applyColor("      &7Remove items from shop."));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop additem <shop_name> <slot> <price>"));
			sender.sendMessage(ColorUtils.applyColor("      &7Add purchasable items in shop."));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop discount <shop_name> <percentage> <time>"));
			sender.sendMessage(ColorUtils.applyColor("      &7Add discounts to shops."));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			if(sender instanceof Player) {
				JSONMessage.create(ColorUtils.applyColor("&e&m---------------&6<<<<<<")).tooltip(ColorUtils.applyColor("&fClick for previous page.")).runCommand("/tokenshop help 2")
				.then(ColorUtils.applyColor("&e&m-----&7|&e3&7|&e&m-----")).tooltip(ColorUtils.applyColor("&fPage 3 of 4."))
				.then(ColorUtils.applyColor("&6>>>>>>&e&m--------------")).tooltip(ColorUtils.applyColor("&fClick for next page.")).runCommand("/tokenshop help 4").send((Player)sender);
			
			}
		}
		else if(sub[1].equalsIgnoreCase("4")) {
			sender.sendMessage(ColorUtils.applyColor("&e&m--------------&6[&e&lEpicset-Tokens&7(4/4)&6]&e&m---------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop resetlimit <player> <shop name> <slot>"));
			sender.sendMessage(ColorUtils.applyColor("      &7Reset limit for a player."));
			if(sender instanceof Player) {
				JSONMessage.create(ColorUtils.applyColor("&e&m---------------&6<<<<<<")).tooltip(ColorUtils.applyColor("&fClick for previous page.")).runCommand("/tokenshop help 3")
				.then(ColorUtils.applyColor("&e&m-----&7|&e4&7|&e&m-----")).tooltip(ColorUtils.applyColor("&fPage 4 of 4."))
				.then(ColorUtils.applyColor("&6>>>>>>&e&m--------------")).send((Player)sender);
			
			}
		}
		else {
			sender.sendMessage(ColorUtils.applyColor("&e&m--------------&6[&e&lEpicset-Tokens&7(1/4)&6]&e&m---------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop  &7Open main shop"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop <shop_name>  &7Open shop by name."));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop create <shop_name>  &7Create a new shop."));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&7&m---------------------------------------------------"));
			sender.sendMessage(ColorUtils.applyColor(" "));
			sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop delete <shop_name>  &7Delete a shop."));
			sender.sendMessage(ColorUtils.applyColor(" "));
			if(sender instanceof Player) {
				JSONMessage.create(ColorUtils.applyColor("&e&m---------------&6<<<<<<"))
				.then(ColorUtils.applyColor("&e&m-----&7|&e1&7|&e&m-----")).tooltip(ColorUtils.applyColor("&fPage 1 of 4."))
				.then(ColorUtils.applyColor("&6>>>>>>&e&m--------------")).tooltip(ColorUtils.applyColor("&fClick for next page.")).runCommand("/tokenshop help 2").send((Player)sender);
			
			}
		}
		return;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		return new ArrayList<String>();
	}
}
