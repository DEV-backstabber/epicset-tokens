package me.backstabber.epicsettokens.commands.sub.tokenshop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.builder.ShopBuilder;
import me.backstabber.epicsettokens.commands.SubShopCommands;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;


public class RemoveCommand extends SubShopCommands {
	private EpicShopManager shopManager;
	private String name = "remove";

	public RemoveCommand(EpicSetTokens plugin,EpicShopManager shopManager) {
		super(plugin);
		this.shopManager=shopManager;
	}
	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender))
			return;
		onCommandByConsole(sender, sub);
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		if(sub.length<3) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cIncorrect usage."));
			sendHelp(sender);
			return;
		}
		String shopName=sub[1].toLowerCase();
		if(!shopManager.isShop(shopName)) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cNo shop by that name found."));
			return;
		}
		int slot=0;
		try {
			slot=Integer.valueOf(sub[2]);
		} catch(NumberFormatException | NullPointerException e) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cPlease enter correct slot number."));
			return;
		}
		ShopBuilder.edit(shopName).removeItem(slot).getShop();
		shopManager.removeSlot(shopManager.getShop(shopName), slot,true);
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&aSucessfully removed an item from slot "+slot+" of "+CommonUtils.StringUtils.format(shopName)+" shop."));
		return;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		List<String> result = new ArrayList<>();
		List<String> options =new ArrayList<String>();
		if(length==2) {
			for(String s:shopManager.getAllShops())
					options.add(s.toLowerCase());
		}
		if(length==3) {
			for(int i=1;i<=9*6;i++)
				options.add(i+"");
		}
		StringUtil.copyPartialMatches(hint, options, result);
		Collections.sort(result);
		return result;
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fRemove an item from shop:"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop remove <shop name> <slot>"));
		sender.sendMessage(ColorUtils.applyColor("&7Dont use spaces instead use \"_\" for space."));
		
	}
}
