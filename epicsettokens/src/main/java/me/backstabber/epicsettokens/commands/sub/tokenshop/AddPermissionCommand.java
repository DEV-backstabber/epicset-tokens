package me.backstabber.epicsettokens.commands.sub.tokenshop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.builder.ShopBuilder;
import me.backstabber.epicsettokens.commands.SubShopCommands;
import me.backstabber.epicsettokens.permission.EpicShopPermission;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.ColorUtils;


public class AddPermissionCommand extends SubShopCommands {
	private EpicShopManager shopManager;
	private String name = "addpermission";

	public AddPermissionCommand(EpicSetTokens plugin,EpicShopManager shopManager) {
		super(plugin);
		this.shopManager=shopManager;
	}
	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender))
			return;
		onCommandByConsole(sender, sub);
		
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		if(sub.length<4) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cIncorrect usage."));
			sendHelp(sender);
			return;
		}
		String shopName=sub[1].toLowerCase();
		if(!shopManager.isShop(shopName)) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cNo shop by that name found."));
			return;
		}
		String perm=sub[2];
		String messagePacked=sub[3];
		for(int i=4;i<sub.length;i++)
			messagePacked=messagePacked+" "+sub[i];
		List<String> message=new ArrayList<String>();
		for(String s: messagePacked.split("\n"))
			message.add(s);
		EpicShopPermission permission = new EpicShopPermission(perm, message);
		ShopBuilder.edit(shopName).addPermission(permission).getShop();
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&aSucessfully added permission node "+perm+" to shop "+ColorUtils.applyColor(shopName)+" with message:"));
		for(String s:message)
			sender.sendMessage(ColorUtils.applyColor(s.replace("%perm%", perm)));
		return;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		List<String> result = new ArrayList<>();
		List<String> options =new ArrayList<String>();
		if(length==2) {
			for(String s:shopManager.getAllShops())
					options.add(s.toLowerCase());
		}
		StringUtil.copyPartialMatches(hint, options, result);
		Collections.sort(result);
		return result;
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fAdd permission to a shop :"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop addpermission <shop name> <permission> <message>"));
		sender.sendMessage(ColorUtils.applyColor("&7Dont use spaces instead use \"_\" for space."));
		sender.sendMessage(ColorUtils.applyColor("&7Use \"\n\" for new line in message."));
		sender.sendMessage(ColorUtils.applyColor("&7Use \"%perm%\" as placeholder for permission node."));
		
	}
}
