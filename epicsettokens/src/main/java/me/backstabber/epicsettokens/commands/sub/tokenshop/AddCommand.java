package me.backstabber.epicsettokens.commands.sub.tokenshop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.builder.ShopBuilder;
import me.backstabber.epicsettokens.api.builder.ShopItemBuilder;
import me.backstabber.epicsettokens.api.shops.ShopTriggers;
import me.backstabber.epicsettokens.commands.SubShopCommands;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;


public class AddCommand extends SubShopCommands {
	private EpicShopManager shopManager;
	private String name = "add";

	public AddCommand(EpicSetTokens plugin,EpicShopManager shopManager) {
		super(plugin);
		this.shopManager=shopManager;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender))
			return;
		if(sub.length<4) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cIncorrect usage."));
			sendHelp(sender);
			return;
		}
		String shopName=sub[1].toLowerCase();
		if(!shopManager.isShop(shopName)) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cNo shop by that name found."));
			return;
		}
		int slot=0;
		try {
			slot=Integer.valueOf(sub[2]);
		} catch(NumberFormatException | NullPointerException e) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cPlease enter correct slot number."));
			return;
		}
		slot=Math.abs(slot);
		ShopTriggers trigger=null;
		try {
			trigger=ShopTriggers.valueOf(sub[3].toUpperCase());
		} catch (Exception e) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cPlease enter correct trigger. (NONE, OPEN_SHOP,CLOSE_GUI)"));
			return;
		}
		ItemStack display=sender.getInventory().getItemInHand();
		if(display==null||display.getType().equals(Material.AIR)) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cPlease Hold an item to use as display."));
			return;
		}
		ShopItemBuilder builder=ShopItemBuilder.create(display);
		builder.setTrigger(trigger);
		if(trigger.equals(ShopTriggers.OPEN_SHOP)) {
			if(sub.length<5) {
				sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cPlease Enter next shop name to use Open_Gui trigger."));
				return;
			}
			String nextShop=sub[4].toLowerCase();
			if(!shopManager.isShop(nextShop)) {
				sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cNo shop by name"+CommonUtils.StringUtils.format(nextShop)+" found."));
				return;
			}
			builder.setClickShop(shopManager.getShop(nextShop));
		}
		ShopBuilder.edit(shopName).addItem(slot, builder).getShop();
		shopManager.removeSlot(shopManager.getShop(shopName), slot,true);
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&aSucessfully added an item to "+CommonUtils.StringUtils.format(shopName)+" shop."));
		return;
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cCommand not supported by console yet."));
		return;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		List<String> result = new ArrayList<>();
		List<String> options =new ArrayList<String>();
		if(length==2) {
			for(String s:shopManager.getAllShops())
					options.add(s.toLowerCase());
		}
		if(length==3) {
			for(int i=1;i<=9*6;i++)
				options.add(i+"");
		}
		if(length==4) {
			for(ShopTriggers triggers:ShopTriggers.values())
				if(!triggers.equals(ShopTriggers.ITEM))
					options.add(triggers.name().toLowerCase());
		}
		if(length==5) {
			for(String s:shopManager.getAllShops())
					options.add(s.toLowerCase());
		}
		StringUtil.copyPartialMatches(hint, options, result);
		Collections.sort(result);
		return result;
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fAdd a non buyable item in shop :"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop add <shop name> <slot> <trigger>"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop add <shop name> <slot> <trigger> <next shop>"));
		sender.sendMessage(ColorUtils.applyColor("&7Dont use spaces instead use \"_\" for space."));
		
	}
}
