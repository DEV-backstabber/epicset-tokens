package me.backstabber.epicsettokens.commands.sub.tokenshop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.builder.ShopBuilder;
import me.backstabber.epicsettokens.api.builder.ShopItemBuilder;
import me.backstabber.epicsettokens.api.shops.ShopTriggers;
import me.backstabber.epicsettokens.commands.SubShopCommands;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.ColorUtils;


@SuppressWarnings("deprecation")
public class AddItemCommand extends SubShopCommands implements Listener {
	private EpicShopManager shopManager;
	private String name = "additem";
	private Map<UUID, ShopItemBuilder> building=new HashMap<UUID, ShopItemBuilder>();
	public AddItemCommand(EpicSetTokens plugin,EpicShopManager shopManager) {
		super(plugin);
		this.shopManager=shopManager;
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender))
			return;
		if(sub.length<4) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cIncorrect usage."));
			sendHelp(sender);
			return;
		}
		String shopName=sub[1].toLowerCase();
		if(!shopManager.isShop(shopName)) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cNo shop by that name found."));
			return;
		}
		int slot=0;
		try {
			slot=Integer.valueOf(sub[2]);
		} catch(NumberFormatException | NullPointerException e) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cPlease enter correct slot number."));
			return;
		}
		int price=0;
		try {
			price=Integer.valueOf(sub[3]);
		} catch(NumberFormatException | NullPointerException e) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cPlease enter correct price."));
			return;
		}
		slot=Math.abs(slot);
		price=Math.abs(price);
		ItemStack display=sender.getInventory().getItemInHand();
		if(display==null||display.getType().equals(Material.AIR)) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cPlease Hold an item to use as display."));
			return;
		}
		ShopItemBuilder builder=ShopItemBuilder.create(display).setTrigger(ShopTriggers.ITEM).setPrice(price);
		building.put(sender.getUniqueId(), builder);
		sender.setMetadata("EpicTokenBuilding", new FixedMetadataValue(plugin, "1:"+shopName));
		sender.setMetadata("EpicTokenSlot", new FixedMetadataValue(plugin, slot+""));
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fStarting building item."));
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fPlease enter purchase limit for this item. (0 for none)"));
		return;
	}
	@EventHandler
	public void onAsyncChat(AsyncPlayerChatEvent event) {
		Player player=event.getPlayer();
		if(player.hasMetadata("EpicTokenBuilding")) {
			event.setCancelled(true);
		}
	}
	//TODO make this async
	@EventHandler
	public void onChat(PlayerChatEvent event) {
		Player player=event.getPlayer();
		if(player.hasMetadata("EpicTokenBuilding")) {
			if(building.containsKey(player.getUniqueId())) {
				event.setCancelled(true);
				String message=event.getMessage();
				String pos=player.getMetadata("EpicTokenBuilding").get(0).asString();
				if(pos.startsWith("1:")) {
					try {
						int limit=Integer.valueOf(message);
						limit=Math.abs(limit);
						if(limit>0)
							building.get(player.getUniqueId()).setLimit(Integer.valueOf(message));
					}catch (NullPointerException | NumberFormatException e) {
						player.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cEnter a number for the limit."));
						return;
					}
					pos="2"+pos.substring(1);
					player.setMetadata("EpicTokenBuilding", new FixedMetadataValue(plugin, pos));
					player.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fGive the display item as reward? (yes/no)"));
					return;
				}
				if(pos.startsWith("2:")) {
					if(message.equalsIgnoreCase("yes"))
						building.get(player.getUniqueId()).giveDisplayItem(true);
					else if(message.equalsIgnoreCase("no"))
						building.get(player.getUniqueId()).giveDisplayItem(false);
					else {
						player.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cEnter yes or no"));
						return;
					}
					pos="3"+pos.substring(1);
					player.setMetadata("EpicTokenBuilding", new FixedMetadataValue(plugin, pos));
					player.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fEnter commands to execute on purchase. Enter end to stop builder."));
					player.sendMessage(ColorUtils.applyColor("&7Begin with <ccmd> to execute via console."));
					player.sendMessage(ColorUtils.applyColor("&7Begin with <pcmd> to execute by player."));
					player.sendMessage(ColorUtils.applyColor("&7Begin with <pmsg> to send player message."));
					player.sendMessage(ColorUtils.applyColor("&7Simple message is broadcasted to server."));
					player.sendMessage(ColorUtils.applyColor("&7Use placeholders %player% for player , %token% for price , %item% for item name."));
					return;
				}
				if(pos.startsWith("3:")) {
					if(message.equalsIgnoreCase("end")) {
						String shopName=pos.substring(2);
						if(player.hasMetadata("EpicTokenSlot")) {
							ShopBuilder.edit(shopName).addItem(Integer.valueOf(player.getMetadata("EpicTokenSlot").get(0).asString()), building.get(player.getUniqueId())).getShop();
							shopManager.removeSlot(shopManager.getShop(shopName), Integer.valueOf(player.getMetadata("EpicTokenSlot").get(0).asString()),true);
						}
						player.removeMetadata("EpicTokenSlot", plugin);
						player.removeMetadata("EpicTokenBuilding", plugin);
						player.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&aSucessfully added an item to shop."));
						return;
					}
					building.get(player.getUniqueId()).addCommand(message);
					player.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fAdded a command to item."));
					player.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fType another command or type end to finish."));
					return;
				}
			}
			else {
				event.setCancelled(true);
				player.removeMetadata("EpicTokenBuilding", plugin);
				player.removeMetadata("EpicTokenSlot", plugin);
				player.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cUnable to build item."));
			}
		}
	}
	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cCommand not supported by console yet."));
		return;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		List<String> result = new ArrayList<>();
		List<String> options =new ArrayList<String>();
		if(length==2) {
			for(String s:shopManager.getAllShops())
					options.add(s.toLowerCase());
		}
		if(length==3) {
			for(int i=1;i<=9*6;i++)
				options.add(i+"");
		}
		StringUtil.copyPartialMatches(hint, options, result);
		Collections.sort(result);
		return result;
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fAdd a buyable item in shop :"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop add <shop name> <slot> <price>"));
		sender.sendMessage(ColorUtils.applyColor("&7Dont use spaces instead use \"_\" for space."));
		
	}
}
