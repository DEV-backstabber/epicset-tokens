package me.backstabber.epicsettokens.commands;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.data.TokenData;
import me.backstabber.epicsettokens.mysql.MySqlManager;
import me.backstabber.epicsettokens.utils.ColorUtils;


public abstract class SubCommands {

	protected EpicSetTokens plugin;
	protected MySqlManager sqlManager;
	protected ConfigurationSection messages;
	public SubCommands(EpicSetTokens plugin, MySqlManager sqlManager) {
		this.plugin = plugin;
		this.sqlManager=sqlManager;
		this.messages = plugin.getSettings().getFile().getConfigurationSection("messages");
	}
	public SubCommands(EpicSetTokens plugin) {
		this.plugin = plugin;
	}

	public abstract void onCommandByPlayer(Player sender, String[] sub);

	public abstract void onCommandByConsole(CommandSender sender, String[] sub);

	public abstract String getName();

	public abstract List<String> getAliases();

	public abstract List<String> getCompletor(int length, String hint);

	public boolean hasPermission(CommandSender sender) {
		if(plugin.getSettings().getFile().getBoolean("admin-protection.enabled")) {
			if(sender instanceof Player &&sender.isOp()) {
				List<String> trusted = plugin.getSettings().getFile().getStringList("admin-protection.trusted-users");
				for(String name:trusted) {
					if(((Player)sender).getName().equalsIgnoreCase(name))
						return true;
					if(((Player)sender).getUniqueId().toString().equalsIgnoreCase(name))
						return true;
				}
				for(String s:plugin.getSettings().getFile().getStringList("admin-protection.message-on-block"))
					sender.sendMessage(ColorUtils.applyColor(s));
				return false;
			}
		}
		if (sender.hasPermission("epicsettokens.tokens.admin"))
			return true;
		if (sender.hasPermission("epicsettokens.tokens." + getName()))
			return true;
		for(String message : messages.getStringList("on-permission-deny"))
			sender.sendMessage(ColorUtils.applyColor(message));
		return false;
	}
	public boolean canUse(Player sender) {
		if(plugin.getSettings().getFile().getBoolean("admin-protection.enabled")) {
			if(sender instanceof Player &&sender.isOp()) {
				List<String> trusted = plugin.getSettings().getFile().getStringList("admin-protection.trusted-users");
				for(String name:trusted) {
					if(((Player)sender).getName().equalsIgnoreCase(name))
						return true;
					if(((Player)sender).getUniqueId().toString().equalsIgnoreCase(name))
						return true;
				}
				return false;
			}
		}
		if (sender.hasPermission("epicsettokens.tokens.admin"))
			return true;
		if (sender.hasPermission("epicsettokens.tokens." + getName()))
			return true;
		return false;
	}
	protected List<Integer> getNumbers(List<String> values) {
		List<Integer> result=new ArrayList<Integer>();
		for(String s:values) {
			try {
				int value=Integer.valueOf(s);
				result.add(value);
			}catch (NumberFormatException | NullPointerException e) {
				throw new NumberFormatException(s);
			}
		}
		return result;
	}
	public TokenData getClosestUuid(String name)
	{
		List<TokenData> table=sqlManager.getCached();
		//simple check
		for(TokenData token:table) {
			if(token.getPlayerName().equalsIgnoreCase(name))
				return token;
		}
		//initials check
		for(TokenData token:table) {
			if(token.getPlayerName().toLowerCase().startsWith(name.toLowerCase()))
				return token;
		}
		//mixed check (5x)
		if(name.length()>1) {
			String chunk = name.substring(0,name.length()-1);
			for(int times=1;times<=5;times++) {
				for(TokenData token:table) {
					if(token.getPlayerName().toLowerCase().startsWith(chunk.toLowerCase()))
						return token;
				}
				if(chunk.length()>1)
					chunk = chunk.substring(0,chunk.length()-1);
				else
					return null;
			}
		}
		return null;
	}
}
