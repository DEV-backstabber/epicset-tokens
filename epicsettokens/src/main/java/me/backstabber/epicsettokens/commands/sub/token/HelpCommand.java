package me.backstabber.epicsettokens.commands.sub.token;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.commands.SubCommands;
import me.backstabber.epicsettokens.utils.ColorUtils;


public class HelpCommand extends SubCommands {
	private String name = "help";

	public HelpCommand(EpicSetTokens plugin) {
		super(plugin);
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender))
			return;
		onCommandByConsole(sender, sub);
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		sendHelp(sender);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		return new ArrayList<String>();
	}

	private void sendHelp(CommandSender sender) {
		for(String message : plugin.getSettings().getFile().getStringList("messages.token.help"))
			sender.sendMessage(ColorUtils.applyColor(message));
	}
}
