package me.backstabber.epicsettokens.commands.sub.tokenshop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.builder.ShopBuilder;
import me.backstabber.epicsettokens.commands.SubShopCommands;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;


public class SetnameCommand extends SubShopCommands {
	private EpicShopManager shopManager;
	private String name = "setname";

	public SetnameCommand(EpicSetTokens plugin,EpicShopManager shopManager) {
		super(plugin);
		this.shopManager=shopManager;
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender))
			return;
		onCommandByConsole(sender, sub);
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		if(sub.length<3) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cIncorrect usage."));
			sendHelp(sender);
			return;
		}
		String shopName=sub[1].toLowerCase();
		if(!shopManager.isShop(shopName)) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cNo shop by that name found."));
			return;
		}
		String displayName=sub[2];
		for(int i=3;i<sub.length;i++)
			displayName=displayName+" "+sub[i];
		if(ColorUtils.stripColors(displayName).replace(" ", "").equalsIgnoreCase("")) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cCannot have a blank gui name."));
			return;
		}
		ShopBuilder.edit(shopName).setDisplayName(displayName).getShop();
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&aSet the name of "+CommonUtils.StringUtils.format(shopName)+" to "+displayName+"&r sucessfully."));
		return;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		if(length==2) {
			List<String> result = new ArrayList<>();
			List<String> options =new ArrayList<String>();
			for(String s:shopManager.getAllShops())
					options.add(s.toLowerCase());
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}
		return new ArrayList<String>();
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fSet Name of gui:"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop setname <shop name> <display name>"));
		sender.sendMessage(ColorUtils.applyColor("&7Dont use spaces for shop name use \"_\" instead."));
		sender.sendMessage(ColorUtils.applyColor("&7Display name can have spaces."));
		
	}
}
