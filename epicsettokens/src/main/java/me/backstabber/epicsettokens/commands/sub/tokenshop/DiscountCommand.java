package me.backstabber.epicsettokens.commands.sub.tokenshop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;


import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.builder.ShopBuilder;
import me.backstabber.epicsettokens.commands.SubShopCommands;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.ColorUtils;


public class DiscountCommand extends SubShopCommands {
	private EpicShopManager shopManager;
	private String name = "discount";

	public DiscountCommand(EpicSetTokens plugin,EpicShopManager shopManager) {
		super(plugin);
		this.shopManager=shopManager;
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender))
			return;
		onCommandByConsole(sender, sub);
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		if(sub.length<4) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cIncorrect usage."));
			sendHelp(sender);
			return;
		}
		String shopName=sub[1].toLowerCase();
		if(!shopName.equalsIgnoreCase("all")&&!shopManager.isShop(shopName)) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cNo shop by that name found."));
			return;
		}
		int percentage=0;
		try {
			percentage=Integer.valueOf(sub[2]);
		} catch(NumberFormatException | NullPointerException e) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cEnter correct percentage value."));
			return;
		}
		percentage=Math.abs(percentage);
		if(percentage>=100) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cPercentage cant be 100. (100 means free items -_-)"));
			return;
		}
		int timeSec=0;
		for(int i=3;i<sub.length;i++) {
			int value=0;
			try {
				value=Integer.valueOf(sub[i]);
			}catch (NumberFormatException | NullPointerException e) {
				try {
					value=Integer.valueOf(sub[i].substring(0,sub[i].length()-1));
				}catch (NumberFormatException | NullPointerException e1) {
					sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cUnable to understand time identifier "+sub[i]+"."));
					return;
				}
			}
			if(sub[i].endsWith("d")) {
				timeSec=timeSec+(value*60*60*24);
				continue;
			}
			if(sub[i].endsWith("h")) {
				timeSec=timeSec+(value*60*60);
				continue;
			}
			if(sub[i].endsWith("m")) {
				timeSec=timeSec+(value*60);
				continue;
			}
			if(sub[i].endsWith("s")) {
				timeSec=timeSec+(value);
				continue;
			}
			timeSec=timeSec+(value);
		}
		if(shopName.equalsIgnoreCase("all")) {
			for(String shop:shopManager.getAllShops()) {
				ShopBuilder.edit(shop).addDiscount(percentage, timeSec).getShop();
			}
		}
		else
			ShopBuilder.edit(shopName).addDiscount(percentage, timeSec).getShop();
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&aSucessfully added discount to shop."));
		return;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		List<String> result = new ArrayList<>();
		List<String> options =new ArrayList<String>();
		if(length==2) {
			for(String s:shopManager.getAllShops())
					options.add(s.toLowerCase());
		}
		if(length==3) {
			for(int i=1;i<=99;i++)
				options.add(i+"");
		}
		StringUtil.copyPartialMatches(hint, options, result);
		Collections.sort(result);
		return result;
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fApply a discount to a shop:"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop discount <shop name> <percentage> <time>"));
		sender.sendMessage(ColorUtils.applyColor("&7Dont use spaces instead use \"_\" for space."));
		sender.sendMessage(ColorUtils.applyColor("&7Time is in seconds but you can have identifiers as (10d 9h 8m 7s)"));
		sender.sendMessage(ColorUtils.applyColor("&7You can use all for shop name to apply discount on all shops. (will remove any previous discounts)"));
		sender.sendMessage(ColorUtils.applyColor("&7Example: /tokenshop discount all 50 2d 30m"));
		
	}
}
