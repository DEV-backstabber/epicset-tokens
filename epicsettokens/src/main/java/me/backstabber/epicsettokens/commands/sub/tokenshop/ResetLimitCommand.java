package me.backstabber.epicsettokens.commands.sub.tokenshop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.commands.SubShopCommands;
import me.backstabber.epicsettokens.data.DataManager;
import me.backstabber.epicsettokens.data.EpicPlayerData;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;


public class ResetLimitCommand extends SubShopCommands {
	private EpicShopManager shopManager;
	private DataManager dataManager;
	private String name = "resetlimit";

	public ResetLimitCommand(EpicSetTokens plugin, EpicShopManager shopManager, DataManager dataManager) {
		super(plugin);
		this.shopManager=shopManager;
		this.dataManager=dataManager;
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender))
			return;
		onCommandByConsole(sender, sub);
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		if(sub.length<3) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cIncorrect usage."));
			sendHelp(sender);
			return;
		}
		if(Bukkit.getPlayerExact(sub[1])==null) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cUnable to find Player "+sub[1]));
			return;
		}
		Player player=Bukkit.getPlayerExact(sub[1]);
		String shopName=sub[2].toLowerCase();
		if(!shopManager.isShop(shopName)) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cNo shop by that name found."));
			return;
		}
		if(sub.length<4) {
			((EpicPlayerData)dataManager.getPlayerData(player)).resetLimit(shopManager.getShop(shopName));
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&aSucessfully resetted limit for "+player.getName()+" in shop "+CommonUtils.StringUtils.format(shopName)));
			return;
		}
		int slot=0;
		try {
			slot=Integer.valueOf(sub[3]);
		} catch(NumberFormatException | NullPointerException e) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cPlease enter correct slot number."));
			return;
		}
		slot=Math.abs(slot);
		((EpicPlayerData)dataManager.getPlayerData(player)).resetLimit(shopManager.getShop(shopName),slot);
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&aSucessfully resetted limit for "+player.getName()+" in shop "+CommonUtils.StringUtils.format(shopName)));
		return;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		List<String> result = new ArrayList<>();
		List<String> options =new ArrayList<String>();
		if(length==2) {
			for(Player player:Bukkit.getOnlinePlayers())
					options.add(player.getName());
		}
		if(length==3) {
			for(String s:shopManager.getAllShops())
				options.add(s.toLowerCase());
		}
		StringUtil.copyPartialMatches(hint, options, result);
		Collections.sort(result);
		return result;
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fReset limits for a player :"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop resetlimit <player> <shop name>"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop resetlimit <player> <shop name> <slot>"));
		sender.sendMessage(ColorUtils.applyColor("&7Dont use spaces instead use \"_\" for space."));
		
	}
}
