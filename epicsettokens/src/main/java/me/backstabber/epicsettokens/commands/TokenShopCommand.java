package me.backstabber.epicsettokens.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;


import me.backstabber.epicsettokens.DependencyInjector;
import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.commands.sub.tokenshop.AddCommand;
import me.backstabber.epicsettokens.commands.sub.tokenshop.AddItemCommand;
import me.backstabber.epicsettokens.commands.sub.tokenshop.AddPermissionCommand;
import me.backstabber.epicsettokens.commands.sub.tokenshop.CreateShopCommand;
import me.backstabber.epicsettokens.commands.sub.tokenshop.DeleteShopCommand;
import me.backstabber.epicsettokens.commands.sub.tokenshop.DiscountCommand;
import me.backstabber.epicsettokens.commands.sub.tokenshop.RemoveCommand;
import me.backstabber.epicsettokens.commands.sub.tokenshop.ResetLimitCommand;
import me.backstabber.epicsettokens.commands.sub.tokenshop.SetbackgroundCommand;
import me.backstabber.epicsettokens.commands.sub.tokenshop.SetnameCommand;
import me.backstabber.epicsettokens.commands.sub.tokenshop.SetsizeCommand;
import me.backstabber.epicsettokens.commands.sub.tokenshop.ShopHelpCommand;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;


public class TokenShopCommand extends DependencyInjector implements CommandExecutor, TabCompleter {
	public TokenShopCommand(EpicSetTokens plugin) {
		super(plugin);
	}

	private List<SubShopCommands> subCommands = new ArrayList<SubShopCommands>();

	public void init(AddItemCommand addItemCommand) {
		subCommands.add(new ShopHelpCommand(plugin));
		subCommands.add(new CreateShopCommand(plugin, (EpicShopManager) shopManager));
		subCommands.add(new DeleteShopCommand(plugin, (EpicShopManager) shopManager));
		subCommands.add(new SetsizeCommand(plugin, (EpicShopManager) shopManager));
		subCommands.add(new SetnameCommand(plugin, (EpicShopManager) shopManager));
		subCommands.add(new SetbackgroundCommand(plugin, (EpicShopManager) shopManager));
		subCommands.add(new AddCommand(plugin, (EpicShopManager) shopManager));
		subCommands.add(new RemoveCommand(plugin, (EpicShopManager) shopManager));
		subCommands.add(new DiscountCommand(plugin, (EpicShopManager) shopManager));
		subCommands.add(addItemCommand);
		subCommands.add(new ResetLimitCommand(plugin, (EpicShopManager) shopManager, dataManager));
		subCommands.add(new AddPermissionCommand(plugin, (EpicShopManager) shopManager));
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] sub) {

		if (sub.length == 0) {
			if(sender instanceof Player&&sender.hasPermission("epicsettokens.tokenshop.open")) 
			{
				shopManager.openShop((Player) sender, shopManager.getShop("main"));
			}
			else
				sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fIncorrect command. Type /tokenshop help."));
			return true;
		}
		for (SubShopCommands subs : subCommands)
			if (sub[0].equalsIgnoreCase(subs.getName()) || subs.getAliases().contains(sub[0].toLowerCase())) {
				if (sender instanceof Player)
					subs.onCommandByPlayer((Player) sender, sub);
				else
					subs.onCommandByConsole(sender, sub);
				return true;
			}
		if(sender instanceof Player&&sender.hasPermission("epicsettokens.tokenshop.open.others")) {
			String shopName=sub[0];
			for(int i=1;i<sub.length;i++)
				shopName=shopName+"_"+sub[i];
			shopName=shopName.toLowerCase();
			if(shopManager.isShop(shopName))
				shopManager.openShop((Player) sender, shopManager.getShop(shopName));
			else
				sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fCannot find that shop."));
		}
		else
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fIncorrect command. Type /tokenshop help."));
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] sub) {
		if (sub.length == 1) {
			List<String> subs = new ArrayList<String>();
			List<String> result = new ArrayList<>();
			for (SubShopCommands command : subCommands) {
				if(sender instanceof Player) {
					if(command.canUse((Player) sender))
						subs.addAll(command.getAliases());
				}
				else
					subs.addAll(command.getAliases());
			}
			for(String s:shopManager.getAllShops())
				subs.add(CommonUtils.StringUtils.format(s));
			StringUtil.copyPartialMatches(sub[0], subs, result);
			Collections.sort(result);
			return result;
		} else if (sub.length > 1) {
			for (SubShopCommands command : subCommands) {
				if (command.getAliases().contains(sub[0].toLowerCase())) {
					return command.getCompletor(sub.length, sub[sub.length - 1]);
				}
			}
		}
		else
			return cmd.getAliases();
		return new ArrayList<>();
	}

}
