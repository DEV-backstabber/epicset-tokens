package me.backstabber.epicsettokens.commands.sub.tokenshop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.builder.ShopBuilder;
import me.backstabber.epicsettokens.commands.SubShopCommands;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;


public class SetbackgroundCommand extends SubShopCommands {
	private EpicShopManager shopManager;
	private String name = "setbackground";

	public SetbackgroundCommand(EpicSetTokens plugin,EpicShopManager shopManager) {
		super(plugin);
		this.shopManager=shopManager;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender))
			return;
		if(sub.length<2) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cIncorrect usage."));
			sendHelp(sender);
			return;
		}
		String shopName=sub[1].toLowerCase();
		if(!shopManager.isShop(shopName)) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cNo shop by that name found."));
			return;
		}
		ItemStack item=sender.getInventory().getItemInHand();
		if(item==null||item.getType().equals(Material.AIR)) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cPlease hold an item to use as background."));
			return;
		}
		ShopBuilder.edit(shopName).setBackground(item.clone()).getShop();
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&aSet "+CommonUtils.BukkitUtils.getItemName(item)+" as background of "+CommonUtils.StringUtils.format(shopName)+" sucessfully."));
		return;
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cCommand not supported by console yet."));
		return;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		if(length==2) {
			List<String> result = new ArrayList<>();
			List<String> options =new ArrayList<String>();
			for(String s:shopManager.getAllShops())
					options.add(s.toLowerCase());
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}
		return new ArrayList<String>();
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fSet background of gui:"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop setbackground <shop name>"));
		sender.sendMessage(ColorUtils.applyColor("&7Dont use spaces for shop name use \"_\" instead."));
		sender.sendMessage(ColorUtils.applyColor("&7Hold an item to use as background."));
		
	}
}
