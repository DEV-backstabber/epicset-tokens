package me.backstabber.epicsettokens.commands.sub.token;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.data.TokenData;
import me.backstabber.epicsettokens.api.events.TokensChangeEvent.ChangeType;
import me.backstabber.epicsettokens.commands.SubCommands;
import me.backstabber.epicsettokens.data.EpicTokenData;
import me.backstabber.epicsettokens.mysql.MySqlManager;
import me.backstabber.epicsettokens.utils.ColorUtils;


public class GiveCommand extends SubCommands {
	private MySqlManager sqlManager;
	private String name = "give";

	public GiveCommand(EpicSetTokens plugin,MySqlManager sqlManager) {
		super(plugin, sqlManager);
		this.sqlManager=sqlManager;
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender))
			return;
		onCommandByConsole(sender, sub);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		if(sub.length<3) {
			for(String message : messages.getStringList(name+".incorrect-usage"))
				sender.sendMessage(ColorUtils.applyColor(message));
			sendHelp(sender);
			return;
		}
		try {
		Integer.valueOf(sub[2]);
		} catch (NumberFormatException e) {
			for(String message : messages.getStringList(name+".incorrect-amount"))
				sender.sendMessage(ColorUtils.applyColor(message.replace("%amount%", sub[2])));
			return;
		}

		TokenData player = getClosestUuid(sub[1]);
		CompletableFuture<TokenData> data = null;
		if(player==null) {
			//test for offline player
			if(Bukkit.getOfflinePlayer(sub[1])==null) {

				for(String message : messages.getStringList(name+".player-not-found"))
					sender.sendMessage(ColorUtils.applyColor(message.replace("%player%", sub[1])));
				return;
			}
			data = sqlManager.getLatest(Bukkit.getOfflinePlayer(sub[1]));
		}
		else
			data = sqlManager.getLatest(player.getUuid());
		data.thenAccept(playerData -> {
			if(playerData==null) {
				for(String message : messages.getStringList(name+".player-not-found"))
					sender.sendMessage(ColorUtils.applyColor(message.replace("%player%", sub[1])));
				return;
			}
			try {
				((EpicTokenData) playerData).setTokens(playerData.getTokens()+Integer.valueOf(sub[2]),ChangeType.COMMAND);
			} catch(IllegalStateException e) {
				for(String message : messages.getStringList(name+".exception"))
					sender.sendMessage(ColorUtils.applyColor(message.replace("%exception%", e.getMessage())));
				return;
			}

			for(String message : messages.getStringList(name+".success"))
				sender.sendMessage(ColorUtils.applyColor(message
						.replace("%amount%", sub[2])
						.replace("%player%", player.getPlayerName())
						.replace("%playerid%", player.getUuid().toString())
						));
			return;
		});
		
		
		
//		new BukkitRunnable() {
//			@SuppressWarnings("deprecation")
//			@Override
//			public void run() {
//				TokenData player = getClosestUuid(sub[1]);
//				CompletableFuture<TokenData> data = null;
//				if(player==null) {
//					//test for offline player
//					if(Bukkit.getOfflinePlayer(sub[1])==null) {
//						sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cCould not find "+sub[1]+" in database."));
//						return;
//					}
//					data = sqlManager.getLatest(Bukkit.getOfflinePlayer(sub[1]));
//				}
//				else
//					data = sqlManager.getLatest(player.getUuid());
//				while(!data.isDone()) {
//					try {Thread.sleep(10);} catch (InterruptedException e) {}
//				}
//				TokenData playerData = data.getNow(null);
//				if(playerData==null) {
//					sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cCould not find "+sub[1]+" in database."));
//					return;
//				}
//				try {
//					((EpicTokenData) playerData).setTokens(playerData.getTokens()+Integer.valueOf(sub[2]),ChangeType.COMMAND);
//				} catch(IllegalStateException e) {
//					sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cError occured while updating tokens."));
//					sender.sendMessage(ColorUtils.applyColor("&c"+e.getMessage()));
//					return;
//				}
//				sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&aSucessfully gave "+sub[2]+" tokens to "+player.getPlayerName()+" &7("+player.getUuid()+")"+"."));
//				return;
//			}
//		}.runTaskAsynchronously(plugin);
		
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		if(length==2) {
			List<String> result = new ArrayList<>();
			List<String> options =new ArrayList<String>();
			for(Player player:Bukkit.getOnlinePlayers())
				options.add(player.getName());
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}
		return new ArrayList<String>();
	}

	private void sendHelp(CommandSender sender) {
		for(String message : messages.getStringList(name+".help"))
			sender.sendMessage(ColorUtils.applyColor(message));
	}
}
