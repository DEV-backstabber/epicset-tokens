package me.backstabber.epicsettokens.commands.sub.tokenshop;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.builder.ShopBuilder;
import me.backstabber.epicsettokens.commands.SubShopCommands;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;


public class CreateShopCommand extends SubShopCommands {
	private EpicShopManager shopManager;
	private String name = "create";

	public CreateShopCommand(EpicSetTokens plugin,EpicShopManager shopManager) {
		super(plugin);
		this.shopManager=shopManager;
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender))
			return;
		onCommandByConsole(sender, sub);
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		if(sub.length<2) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cIncorrect usage."));
			sendHelp(sender);
			return;
		}
		String shopName=sub[1].toLowerCase();
		if(shopManager.isShop(shopName)) {
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&cA Shop by that name exists. Edit or delete that instead."));
			return;
		}
		ShopBuilder.createNew(shopName).setDisplayName(CommonUtils.StringUtils.format(shopName)).getShop();
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&aCreated a shop by name "+CommonUtils.StringUtils.format(shopName)+"."));
		return;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		return new ArrayList<String>();
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fCreate a new shop :"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/tokenshop create <shop name>"));
		sender.sendMessage(ColorUtils.applyColor("&7Dont use spaces instead use \"_\" for space."));
		
	}
}
