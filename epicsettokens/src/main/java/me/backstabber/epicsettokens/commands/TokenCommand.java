package me.backstabber.epicsettokens.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.StringUtil;


import me.backstabber.epicsettokens.DependencyInjector;
import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.data.TokenData;
import me.backstabber.epicsettokens.commands.sub.token.GiveCommand;
import me.backstabber.epicsettokens.commands.sub.token.HelpCommand;
import me.backstabber.epicsettokens.commands.sub.token.ResetCommand;
import me.backstabber.epicsettokens.commands.sub.token.SetCommand;
import me.backstabber.epicsettokens.commands.sub.token.TakeCommand;
import me.backstabber.epicsettokens.mysql.MySqlManager;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;


public class TokenCommand extends DependencyInjector implements CommandExecutor, TabCompleter {
	
	
	public TokenCommand(EpicSetTokens plugin) {
		super(plugin);
	}

	private List<SubCommands> subCommands = new ArrayList<SubCommands>();

	public void init() {
		this.subCommands.add(new GiveCommand(plugin, (MySqlManager) tokenManager));
		this.subCommands.add(new TakeCommand(plugin, (MySqlManager) tokenManager));
		this.subCommands.add(new SetCommand(plugin, (MySqlManager) tokenManager));
		this.subCommands.add(new ResetCommand(plugin, (MySqlManager) tokenManager));
		this.subCommands.add(new HelpCommand(plugin));
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] sub) {
		if (sub.length == 0) {
			if(sender.hasPermission("epicsettokens.tokens.preview")&&sender instanceof Player) 
			{
				Player player=(Player)sender;
				TokenData cache = tokenManager.getCached(player);
				if(cache!=null) {
					int tokens=cache.getTokens();
					for(String s:plugin.getSettings().getFile().getStringList("messages.on-check-self"))
						player.sendMessage(ColorUtils.applyColor(s.replace("%tokens%", CommonUtils.NumericsUtils.formatPrefixedNumbers(tokens)).replace("%player%", player.getName())));
				}
				else {
					((MySqlManager) tokenManager).updateCache(player);
					new BukkitRunnable() {
						@Override
						public void run() {
							int tokens=tokenManager.getCached(player).getTokens();
							for(String s:plugin.getSettings().getFile().getStringList("messages.on-check-self"))
								player.sendMessage(ColorUtils.applyColor(s.replace("%tokens%", CommonUtils.NumericsUtils.formatTwoDecimals(tokens)).replace("%player%", player.getName())));
						}
					}.runTaskLater(plugin, 20*2);
				}
			}
			else
				sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fIncorrect command. Type /token help."));
			return true;
		}
		for (SubCommands subs : subCommands)
			if (sub[0].equalsIgnoreCase(subs.getName()) || subs.getAliases().contains(sub[0].toLowerCase())) {
				if (sender instanceof Player)
					subs.onCommandByPlayer((Player) sender, sub);
				else
					subs.onCommandByConsole(sender, sub);
				return true;
			}
		if(sender.hasPermission("epicsettokens.tokens.preview.others")) {
			new BukkitRunnable() {
				@Override
				public void run() {
					TokenData player = caseChecker.getClosestUuid(sub[0]);
					if(player==null) {
						for(String s:plugin.getSettings().getFile().getStringList("messages.on-check-fail"))
							sender.sendMessage(ColorUtils.applyColor(s.replace("%player%", sub[0])));
						return;
					}
					CompletableFuture<TokenData> data = tokenManager.getLatest(player.getUuid());
					while(!data.isDone()) {
						try {Thread.sleep(10);} catch (InterruptedException e) {}
					}
					TokenData playerData = data.getNow(null);
					if(playerData==null) {
						for(String s:plugin.getSettings().getFile().getStringList("messages.on-check-fail"))
							sender.sendMessage(ColorUtils.applyColor(s.replace("%player%", player.getPlayerName())));
						return;
					}
					int tokens=playerData.getTokens();
					for(String s:plugin.getSettings().getFile().getStringList("messages.on-check-other"))
						sender.sendMessage(ColorUtils.applyColor(s.replace("%tokens%", CommonUtils.NumericsUtils.formatTwoDecimals(tokens)).replace("%player%", playerData.getPlayerName())));
				}
			}.runTaskAsynchronously(plugin);
			
		}
		else
			sender.sendMessage(ColorUtils.applyColor("&6[&e&lEpicset-Tokens&6] &7&l>>&fIncorrect command. Type /token help."));
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] sub) {
		if (sub.length == 1) {
			List<String> subs = new ArrayList<String>();
			List<String> result = new ArrayList<>();
			for (SubCommands command : subCommands) {
				if(sender instanceof Player) {
					if(command.canUse((Player) sender))
						subs.addAll(command.getAliases());
				}
				else
					subs.addAll(command.getAliases());
			}
			if(sender.hasPermission("epicsettokens.tokens.preview.others"))
				for(Player player:Bukkit.getOnlinePlayers())
					subs.add(player.getName());
			StringUtil.copyPartialMatches(sub[0], subs, result);
			Collections.sort(result);
			return result;
		} else if (sub.length > 1) {
			for (SubCommands command : subCommands) {
				if (command.getAliases().contains(sub[0].toLowerCase())) {
					return command.getCompletor(sub.length, sub[sub.length - 1]);
				}
			}
		}
		else
			return cmd.getAliases();
		return new ArrayList<>();
	}

}
