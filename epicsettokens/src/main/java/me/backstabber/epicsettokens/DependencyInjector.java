package me.backstabber.epicsettokens;


import me.backstabber.epicsettokens.api.managers.ShopManager;
import me.backstabber.epicsettokens.api.managers.TokensManager;
import me.backstabber.epicsettokens.commands.TokenCommand;
import me.backstabber.epicsettokens.commands.TokenShopCommand;
import me.backstabber.epicsettokens.data.DataManager;
import me.backstabber.epicsettokens.limits.LimitResetter;
import me.backstabber.epicsettokens.listeners.PlayerListener;
import me.backstabber.epicsettokens.mysql.MySqlManager;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.CaseChecker;

/**
 * This is the custom binder module for 
 * Google Guice dependency injection
 * @author Backstabber
 *
 */
public class DependencyInjector {
	
	//singleton instances
	
	 protected EpicSetTokens plugin;
	 protected TokensManager tokenManager;
	 protected ShopManager shopManager;
	 protected DataManager dataManager;
	 protected CaseChecker caseChecker;
	 
	 //non singleton instances
	 
	 protected PlayerListener playerListener;
	 protected TokenCommand tokenCommand;
	 protected TokenShopCommand tokenShopCommand;
	 protected LimitResetter resetter;
	 public DependencyInjector(EpicSetTokens plugin)
	 {
	     if(plugin!=null)
	    	 plugin.injectMembers(this);
	 }
	 public void init(EpicSetTokens plugin) {
		 this.plugin = plugin;
		 this.tokenManager=new MySqlManager(plugin);
		 this.caseChecker=new CaseChecker(plugin);
		 this.dataManager=new DataManager(plugin);
		 this.shopManager=new EpicShopManager(plugin);
		 

		 this.playerListener=new PlayerListener(plugin);
		 this.tokenCommand=new TokenCommand(plugin);
		 this.tokenShopCommand=new TokenShopCommand(plugin);
	     this.resetter=new LimitResetter(plugin);
	 }
	 /**
	  * Function to inject all dependencies to
	  * an object 
	  * @param instance
	  */
	 public void injectMembers(Object instance) 
	 {
		 if(instance instanceof DependencyInjector) {
			 DependencyInjector injectable=(DependencyInjector) instance;
		     injectable.plugin=this.plugin;
		     injectable.tokenManager=this.tokenManager;
		     injectable.shopManager=this.shopManager;
		     injectable.dataManager=this.dataManager;
		     injectable.caseChecker=this.caseChecker;
		     

			 injectable.playerListener=this.playerListener;
			 injectable.tokenCommand=this.tokenCommand;
			 injectable.tokenShopCommand=this.tokenShopCommand;
		     injectable.resetter=this.resetter;
		 }
	 }
}