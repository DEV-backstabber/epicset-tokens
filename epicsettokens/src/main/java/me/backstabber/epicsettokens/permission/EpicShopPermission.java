package me.backstabber.epicsettokens.permission;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import me.backstabber.epicsettokens.api.permission.ShopPermission;
import me.backstabber.epicsettokens.api.shops.ShopPaths;
import me.backstabber.epicsettokens.utils.ColorUtils;

public class EpicShopPermission implements ShopPermission {
	private List<String> failMessage=new ArrayList<String>();
	private String node;
	public EpicShopPermission(String node,List<String> failMessage) {
		this.node=node;
		this.failMessage=failMessage;
	}
	public EpicShopPermission(String node,String failMessage) {
		this.node=node;
		this.failMessage.add(failMessage);
	}
	@Override
	public boolean hasPermission(Player player) {
		return player.hasPermission(this.node);
	}
	@Override
	public void sendFailMessage(Player player) {
		for(String msg:failMessage)
			player.sendMessage(ColorUtils.applyColor(msg.replace("%perm%", node)));
	}
	@Override
	public boolean checkPermission(Player player) {
		if(player.hasPermission(node))
			return true;
		for(String msg:failMessage)
			player.sendMessage(ColorUtils.applyColor(msg.replace("%perm%", node)));
		return false;
	}
	public void save(FileConfiguration file) {
		file.set(ShopPaths.PERMISSION.getPath(), node);
		file.set(ShopPaths.PERMISSION_MESSAGE.getPath(), failMessage);
	}
}
