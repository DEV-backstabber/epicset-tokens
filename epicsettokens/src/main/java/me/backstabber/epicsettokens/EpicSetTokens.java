package me.backstabber.epicsettokens;


import java.util.List;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;


import me.backstabber.epicsettokens.api.TokensApi;
import me.backstabber.epicsettokens.api.data.PlayerData;
import me.backstabber.epicsettokens.api.managers.ShopManager;
import me.backstabber.epicsettokens.api.managers.TokensManager;
import me.backstabber.epicsettokens.api.permission.ShopPermission;
import me.backstabber.epicsettokens.commands.sub.tokenshop.AddItemCommand;
import me.backstabber.epicsettokens.hooks.PapiHook;
import me.backstabber.epicsettokens.limits.LimitResetter;
import me.backstabber.epicsettokens.limits.RefreshType;
import me.backstabber.epicsettokens.mysql.MySqlManager;
import me.backstabber.epicsettokens.permission.EpicShopPermission;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;
import me.backstabber.epicsettokens.utils.EpicCommand;
import me.backstabber.epicsettokens.utils.YamlManager;



public class EpicSetTokens extends JavaPlugin {
	/**
	 * Declaring non injectable fields
	 */
	private AddItemCommand addItemCommand;
	private YamlManager config;
	private YamlManager limitSettings;
	private ConsoleCommandSender console = Bukkit.getConsoleSender();
	
	
	
	
	
	
	private DependencyInjector dependencies;
	private EpicCommand tokenCommand;
	private EpicCommand tokenShopCommand;
	@Override
	public void onEnable() {
		console.sendMessage(ColorUtils.applyColor("&e-----------------------------------------------------------------------------------------"));
		//load config
		config=new YamlManager(this, "config");
		limitSettings=new YamlManager(this, "limit settings");
		//build injection
		dependencies=new DependencyInjector(null);
		dependencies.init(this);
		//setup managers
		console.sendMessage(ColorUtils.applyColor("&a[EpicSet-Tokens] >&aAttempting to connect to the Database."));
		console.sendMessage(ColorUtils.applyColor("&e-----------------------------------------------------------------------------------------"));
		((MySqlManager) dependencies.tokenManager).build();
	}
	@Override
	public void onDisable() {
		if(tokenCommand!=null && tokenCommand.isRegistered()) {
			tokenCommand.unregister();
			CommonUtils.NmsUtils.unRegisterCommand(tokenCommand, this);
		}
		if(tokenShopCommand!=null && tokenShopCommand.isRegistered()) {
			tokenShopCommand.unregister();
			CommonUtils.NmsUtils.unRegisterCommand(tokenShopCommand, this);
		}
		
		//stop all tasks
		Bukkit.getScheduler().cancelTasks(this);
		//close all open shops
		//remove meta from players & close all shops
		for(Player player:Bukkit.getOnlinePlayers()) {
			//remove meta
			if(player.hasMetadata("EpicTokenBuilding"))
				player.removeMetadata("EpicTokenBuilding", this);
			if(player.hasMetadata("EpicTokenSlot"))
				player.removeMetadata("EpicTokenSlot", this);
			//close shops
			if(dependencies.shopManager.isShopOpen(player))
				dependencies.shopManager.closeShop(player);
			console.sendMessage(ColorUtils.applyColor("&e-----------------------------------------------------------------------------------------"));
			
		}
		//save player's data
		dependencies.dataManager.saveAll();
	}
	public void connected() {
		((MySqlManager) dependencies.tokenManager).init();
		dependencies.tokenCommand.init();
		addItemCommand=new AddItemCommand(this, (EpicShopManager) dependencies.shopManager);
		dependencies.tokenShopCommand.init(addItemCommand);
		//load default shop
		if(!dependencies.shopManager.isShop("main")) {
			new YamlManager(this, "main","Shops");
			new YamlManager(this, "armor","Shops");
		}
		//start up limit refresh timing
		for(RefreshType type:RefreshType.values())
			dependencies.resetter.startRefreshTimer(type);
		//setup listeners
		Bukkit.getPluginManager().registerEvents(dependencies.playerListener, this);
		Bukkit.getPluginManager().registerEvents(addItemCommand, this);
		//setup commands
		getCommand("token").setExecutor(dependencies.tokenCommand);
		getCommand("token").setTabCompleter(dependencies.tokenCommand);
		getCommand("tokenshop").setExecutor(dependencies.tokenShopCommand);
		getCommand("tokenshop").setTabCompleter(dependencies.tokenShopCommand);
		//swap commands if enabled
		if(config.getFile().getBoolean("command-settings.use-alternate-commands")) {
			console.sendMessage(ColorUtils.applyColor("&a[EpicSet-Tokens] >&aSwapping Default Commands."));
			console.sendMessage(ColorUtils.applyColor("&a[EpicSet-Tokens] >&aSwapping &eTokens &r&a Command"));
			console.sendMessage(ColorUtils.applyColor("&e-----------------------------------------------------------------------------------------"));
			CommonUtils.NmsUtils.unRegisterCommand(getCommand("token"), this);
			String newName = config.getFile().getString("command-settings.token.main");
			List<String> newAliases = config.getFile().getStringList("command-settings.token.aliases");
			
			tokenCommand = createCommand(newName,newAliases, dependencies.tokenCommand, dependencies.tokenCommand);
			console.sendMessage(ColorUtils.applyColor("&a[EpicSet-Tokens] >&aCommand Swapped."));
			console.sendMessage(ColorUtils.applyColor("&a[EpicSet-Tokens] >&aSwapping &eTokenShop &r&a Command"));
			console.sendMessage(ColorUtils.applyColor("&e-----------------------------------------------------------------------------------------"));

			CommonUtils.NmsUtils.unRegisterCommand(getCommand("tokenshop"), this);
			String newShopName = config.getFile().getString("command-settings.tokenshop.main");
			List<String> newShopAliases = config.getFile().getStringList("command-settings.tokenshop.aliases");
			tokenShopCommand = createCommand(newShopName, newShopAliases, dependencies.tokenShopCommand, dependencies.tokenShopCommand);		
			
			console.sendMessage(ColorUtils.applyColor("&a[EpicSet-Tokens] >&aCommand Swapped."));
			console.sendMessage(ColorUtils.applyColor("&e-----------------------------------------------------------------------------------------"));

	 		tokenCommand.register();
			tokenShopCommand.register();
		}
		
		//register api
		Api api=new Api();
		injectMembers(api);
		Bukkit.getServicesManager().register(TokensApi.class, api, this, ServicePriority.Normal);
		//setup hooks
		if(Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
			PapiHook papiHook = new PapiHook(this, (MySqlManager) dependencies.tokenManager);
			injectMembers(papiHook);
			papiHook.init();
		}
		console.sendMessage(ColorUtils.applyColor("&e-----------------------------------------------------------------------------------------"));
	}
	public void disconnected() {
		Bukkit.getPluginManager().disablePlugin(this);
	}
	/**
	 * Function to get config files
	 * Since I use Yamlmanager instead of spigot's
	 * default yamlconfiguration this is used
	 * @return
	 */
	public YamlManager getSettings() {
		return this.config;
	}
	public YamlManager getLimitSettings() {
		return this.limitSettings;
	}
	/**
	 * Gets the shop manager to manage tokenshops
	 * @return
	 */
	public EpicShopManager getShopManager() {
		return (EpicShopManager) dependencies.shopManager;
	}
	public void injectMembers(Object instance) {
		try {
		dependencies.injectMembers(instance);
		}catch (NullPointerException e) {
			getLogger().log(Level.INFO,"Tried to inject data into null object "+instance.getClass().getSimpleName());
		}
	}
	
	private class Api implements TokensApi {
		/**
		 * Fetch an instance of the SqlManager used
		 * for storing & updating player's tokens
		 * @return MySqlManager instance
		 */
		public TokensManager getTokensManager() {
			return dependencies.tokenManager;
		}
		/**
		 * Fetch shopmanager instance used to store
		 * open & close tokenshops for players
		 * @return ShopManager instance
		 */
		public ShopManager getShopManager() {
			return dependencies.shopManager;
		}
		public PlayerData getPlayerData(Player player) {
			return dependencies.dataManager.getPlayerData(player);
		}
		@Override
		public ShopPermission createPermission(String permission, List<String> message) {
			return new EpicShopPermission(permission, message);
		}
		@Override
		public ShopPermission createPermission(String permission, String message) {
			return new EpicShopPermission(permission, message);
		}
		
	}
	public LimitResetter getResetter() {
		return dependencies.resetter;
	}
	public EpicCommand createCommand(String name , List<String> aliases , CommandExecutor executor , TabCompleter completor) {
		return new EpicCommand(name, aliases, executor, completor);
	}

}
