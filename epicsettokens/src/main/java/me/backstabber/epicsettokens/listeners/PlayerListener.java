package me.backstabber.epicsettokens.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryCreativeEvent;
import org.bukkit.event.player.PlayerJoinEvent;


import me.backstabber.epicsettokens.DependencyInjector;
import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.mysql.MySqlManager;
import me.backstabber.epicsettokens.shops.EpicTokenShop;

public class PlayerListener extends DependencyInjector implements Listener {
	public PlayerListener(EpicSetTokens plugin) {
		super(plugin);
	}
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		((MySqlManager) tokenManager).updateCache(event.getPlayer());
	}
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if(event instanceof InventoryCreativeEvent)
			return;
		if(shopManager.isShopOpen((Player) event.getWhoClicked()))
			((EpicTokenShop) shopManager.getOpenShop((Player) event.getWhoClicked())).handleClick(event);
	}
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		if(shopManager.isShopOpen((Player) event.getPlayer()))
			((EpicTokenShop) shopManager.getOpenShop((Player) event.getPlayer())).handleClose(event);
	}
}
