package me.backstabber.epicsettokens.data;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;


import me.backstabber.epicsettokens.DependencyInjector;
import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.data.PlayerData;
import me.backstabber.epicsettokens.api.shops.TokenShop;
import me.backstabber.epicsettokens.utils.YamlManager;

public class DataManager extends DependencyInjector {
	public DataManager(EpicSetTokens plugin) {
		super(plugin);
	}

	private Map<UUID, PlayerData> data=new HashMap<UUID, PlayerData>();
	public PlayerData getPlayerData(Player player) {
		if(!data.containsKey(player.getUniqueId()))
			data.put(player.getUniqueId(), EpicPlayerData.getData(player));
		return data.get(player.getUniqueId());
	}

	public void removeSlot(TokenShop shop,int slot,boolean removeCounts) {
		new BukkitRunnable() {
			@Override
			public void run() {
				for(PlayerData playerData:data.values()) {
					((EpicPlayerData)playerData).resetLimit(shop, slot,removeCounts);
				}
				//reset for unloaded players as well
				for(String fileName:getAllFiles("PlayerData")) {
					YamlManager file=new YamlManager(plugin, fileName,"PlayerData");
					if(removeCounts)
					file.getFile().set(shop.getName()+"."+slot, null);
					else {
						file.getFile().set(shop.getName()+"."+slot+".limit-count", null);
						file.getFile().set(shop.getName()+"."+slot+".delayed", null);
					}
					file.save(false);
				}
			}
		}.runTaskAsynchronously(plugin);
	}
	public void removeSlot(TokenShop shop) {
		new BukkitRunnable() {
			@Override
			public void run() {
				for(PlayerData playerData:data.values()) {
					((EpicPlayerData)playerData).resetLimit(shop);
				}
				//reset for unloaded players as well
				for(String fileName:getAllFiles("PlayerData")) {
					YamlManager file=new YamlManager(plugin, fileName,"PlayerData");
					file.getFile().set(shop.getName(), null);
					file.save(false);
				}
			}
		}.runTaskAsynchronously(plugin);
		
	}
	public void saveAll() {
		for(PlayerData playerData:data.values()) {
			((EpicPlayerData)playerData).save();
		}
		
	}

	private List<String> getAllFiles(String path) {
		File folder = new File(plugin.getDataFolder() + "/" + path);
		String[] fileNames = folder.list();
		ArrayList<String> names = new ArrayList<String>();
		if (fileNames != null) {
			for (String s : fileNames) {
				names.add(s.replace(".yml", ""));
			}
		}
		return names;
	}
}
