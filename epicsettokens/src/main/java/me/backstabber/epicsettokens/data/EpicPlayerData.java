package me.backstabber.epicsettokens.data;


import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.data.PlayerData;
import me.backstabber.epicsettokens.api.shops.ShopItem;
import me.backstabber.epicsettokens.api.shops.TokenShop;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;
import me.backstabber.epicsettokens.utils.YamlManager;

public class EpicPlayerData implements PlayerData{
	private YamlManager file;
	private Player player;
	static EpicPlayerData getData(Player player) {
		EpicSetTokens plugin = EpicSetTokens.getPlugin(EpicSetTokens.class);
		EpicPlayerData data=new EpicPlayerData();
		data.file=new YamlManager(plugin, player.getUniqueId().toString(),"PlayerData");
		data.player=player;
		return data;
	}
	@Override
	public int getPurchases(TokenShop shop,ShopItem item) {
		if(!file.getFile().isSet(shop.getName()+"."+item.getSlot()+".count")) {
			file.getFile().set(shop.getName()+"."+item.getSlot()+".count", 0);
			file.save(false);
		}
		return file.getFile().getInt(shop.getName()+"."+item.getSlot()+".count");
	}
	@Override
	public int getLimitPurchases(TokenShop shop,ShopItem item) {
		if(!file.getFile().isSet(shop.getName()+"."+item.getSlot()+".limit-count")) {
			file.getFile().set(shop.getName()+"."+item.getSlot()+".limit-count", 0);
			file.save(false);
		}
		return file.getFile().getInt(shop.getName()+"."+item.getSlot()+".limit-count");
	}
	@Override
	public boolean canPurchase(TokenShop shop,ShopItem item) {
		if(item.getLimit()<=0)
			return true;
		int itemLimit=item.getLimit();
		int purchases=getLimitPurchases(shop, item);
		if(purchases<itemLimit)
			return true;
		return false;
	}
	@Override
	public boolean isDelayed(TokenShop shop,ShopItem item) {
		return file.getFile().isSet(shop.getName()+"."+item.getSlot()+".delayed");
	}
	public void addPurchase(TokenShop shop,ShopItem item) {
		EpicSetTokens plugin = EpicSetTokens.getPlugin(EpicSetTokens.class);
		file.getFile().set(shop.getName()+"."+item.getSlot()+".count", getPurchases(shop, item)+1);
		file.getFile().set(shop.getName()+"."+item.getSlot()+".limit-count", getLimitPurchases(shop, item)+1);
		file.save(false);
		file.getFile().set(shop.getName()+"."+item.getSlot()+".delayed", true);
		new BukkitRunnable() {
			@Override
			public void run() {
				file.getFile().set(shop.getName()+"."+item.getSlot()+".delayed", null);
			}
		}.runTaskLater(plugin, 20*plugin.getSettings().getFile().getInt("shop-settings.purchase-delay"));
		if(!canPurchase(shop, item)&&plugin.getLimitSettings().getFile().getBoolean("settings.send-message-on limit-reaching")) {
			for(String s:plugin.getLimitSettings().getFile().getStringList("settings.limit-message")) {
				s=s.replace("%player%",player.getName());
				s=s.replace("%shop%", CommonUtils.StringUtils.format(shop.getName()));
				s=s.replace("%item%", CommonUtils.BukkitUtils.getItemName(item.getDisplayItem(player)));
				s=s.replace("%slot%", item.getSlot()+"");
				Bukkit.broadcast(ColorUtils.applyColor(s), "epicsettokens.tokenshop.notify");
			}
		}
	}
	@Override
	public void resetLimit(TokenShop shop,ShopItem item) {
		file.getFile().set(shop.getName()+"."+item.getSlot()+".limit-count", null);
		file.getFile().set(shop.getName()+"."+item.getSlot()+".delayed", null);
		file.save(false);
	}
	public void resetLimit(TokenShop shop,int slot) {
		file.getFile().set(shop.getName()+"."+slot+".limit-count", null);
		file.getFile().set(shop.getName()+"."+slot+".delayed", null);
	}
	public void resetLimit(TokenShop shop,int slot,boolean removeCounts) {
		if(removeCounts)
			file.getFile().set(shop.getName()+"."+slot, null);
		else {
			file.getFile().set(shop.getName()+"."+slot+".limit-count", null);
			file.getFile().set(shop.getName()+"."+slot+".delayed", null);
		}
	}
	public void resetLimit(TokenShop shop) {
		ConfigurationSection shopConfig = file.getFile().isConfigurationSection(shop.getName()) ? file.getFile().getConfigurationSection(shop.getName()) : null;
		if(shopConfig!=null) {
			for(String key : shopConfig.getKeys(false)) {
				shopConfig.set(key+".limit-count", null);
				shopConfig.set(key+".delayed", null);
			}
		}
	}
	public FileConfiguration getFile() {
		return this.file.getFile();
	}
	void save() {
		this.file.save(false);
	}
}
