package me.backstabber.epicsettokens.data;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import me.backstabber.epicsettokens.DependencyInjector;
import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.data.TokenData;
import me.backstabber.epicsettokens.api.events.TokensChangeEvent;
import me.backstabber.epicsettokens.api.events.TokensChangeEvent.ChangeType;
import me.backstabber.epicsettokens.mysql.MySqlManager;

public class EpicTokenData extends DependencyInjector implements TokenData {
	private int tokens;
	private UUID uuid;
	private String playerName ;
	private boolean isCached;
	public EpicTokenData(EpicSetTokens plugin, OfflinePlayer player,boolean isCached) {
		super(plugin);
		this.playerName =player.getName();
		this.uuid=player.getUniqueId();
		this.tokens=0;
		this.isCached=isCached;
	}
	public EpicTokenData(EpicSetTokens plugin, Player player,int tokens,boolean isCached) {
		super(plugin);
		this.playerName=player.getName();
		this.uuid=player.getUniqueId();
		this.tokens=tokens;
		this.isCached=isCached;
	}
	public EpicTokenData(EpicSetTokens plugin, UUID uuid,String playerName,boolean isCached) {
		super(plugin);
		this.playerName=playerName;
		this.uuid=uuid;
		this.tokens=0;
		this.isCached=isCached;
	}
	@Override
	public UUID getUuid() {
		return uuid;
	}
	@Override
	public int getTokens() {
		return this.tokens;
	}
	@Override
	public void setTokens(int tokens) {
		setTokens(tokens, ChangeType.API);
	}
	@Override
	public String getPlayerName() {
		return playerName;
	}
	public void setTokens(int tokens,ChangeType type) {
		if(isCached)
			throw new IllegalStateException("Cannot set Tokens to a cached Data.");
		int oldTokens=this.tokens;
		int newTokens=tokens;
		TokensChangeEvent event=new TokensChangeEvent(this, newTokens-oldTokens, type);
		Bukkit.getPluginManager().callEvent(event);
		if(!event.isCancelled()) {
			this.tokens = oldTokens+event.getChange();
			((MySqlManager) tokenManager).setData(this);
		}
	}
	public boolean isCached() {
		return isCached;
	}
	public void updateTokens(int tokens) {
		this.tokens = tokens;
	}
	public void setName(String name) {
		this.playerName=name;
	}
	public int getSize() {
		int size=16+4;
		if(playerName==null)
			return size;
		return size+(playerName.getBytes().length/16);
	}
}
