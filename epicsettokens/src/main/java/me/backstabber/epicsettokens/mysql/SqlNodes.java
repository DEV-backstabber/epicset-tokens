package me.backstabber.epicsettokens.mysql;

import org.bukkit.configuration.file.FileConfiguration;

import me.backstabber.epicsettokens.utils.YamlManager;

public enum SqlNodes {
	HOST("sql-settings.hostname","localhost"),
	DATABASE("sql-settings.database","db"),
	USERNAME("sql-settings.username",""),
	PASSWORD("sql-settings.password",""),
	PORT("sql-settings.port",3306),
	ENABLE_CACHE("sql-settings.enable-cache",true),
	CACHE_SIZE("sql-settings.max-cache-size",100);
	private Object defValue;
	private String node;
	SqlNodes(String node,Object defValue) {
		this.node=node;
		this.defValue=defValue;
	}
	public Object getValue(FileConfiguration file) {
		return file.get(node,defValue);
	}
	public Object getValue(YamlManager manager) {
		return manager.getFile().get(node,defValue);
	}
}
