package me.backstabber.epicsettokens.mysql;

import me.backstabber.epicsettokens.EpicSetTokens;

public class ConnectionBuilder {
	private EpicSetTokens plugin;
	private String host = null;
	private String port = null;
	private String database = null;
	private String username = null;
	private String password = null;
	public static ConnectionBuilder create(EpicSetTokens plugin) {
		ConnectionBuilder builder=new ConnectionBuilder();
		builder.plugin=plugin;
		return builder;
	}
	public ConnectionBuilder setHostName(String hostname) {
		this.host=hostname;
		return this;
	}
	public ConnectionBuilder setPort(int port) {
		this.port=String.valueOf(port);
		return this;
	}
	public ConnectionBuilder setDatabase(String database) {
		this.database=database;
		return this;
	}
	public ConnectionBuilder setUserName(String username) {
		this.username=username;
		return this;
	}
	public ConnectionBuilder setPassword(String password) {
		this.password=password;
		return this;
	}
	public DatabaseConnection getConnection() {
		if(host==null)
			throw new NullPointerException("Hostname was not set.");
		if(port==null)
			throw new NullPointerException("Port was undefined.");
		if(database==null)
			throw new NullPointerException("No database name provided.");
		if(username==null)
			throw new NullPointerException("Unknown username provided.");
		if(password==null)
			throw new NullPointerException("Password is undefined.");
		DatabaseConnection connection=new DatabaseConnection(plugin);
		plugin.injectMembers(connection);
		connection.create(host, port, database, username, password);
		return connection;
	}
}
