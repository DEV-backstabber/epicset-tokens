package me.backstabber.epicsettokens.mysql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;


import me.backstabber.epicsettokens.DependencyInjector;
import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.data.TokenData;
import me.backstabber.epicsettokens.api.events.TokensChangeEvent;
import me.backstabber.epicsettokens.api.events.TokensChangeEvent.ChangeType;
import me.backstabber.epicsettokens.api.managers.TokensManager;
import me.backstabber.epicsettokens.data.EpicTokenData;

public class MySqlManager extends DependencyInjector implements TokensManager  {
	private Map<UUID, TokenData> onlineCache=new ConcurrentHashMap<UUID,TokenData>();
	private Map<UUID, TokenData> offlineCache=new ConcurrentHashMap<UUID,TokenData>();
	private DatabaseConnection connection;
	public MySqlManager(EpicSetTokens plugin) {
		super(plugin);
	}
	public void build() {
		//build a connection with the sql database
		this.connection=ConnectionBuilder.create(plugin)
				.setHostName((String) SqlNodes.HOST.getValue(plugin.getSettings()))
				.setPort((int) SqlNodes.PORT.getValue(plugin.getSettings()))
				.setDatabase((String) SqlNodes.DATABASE.getValue(plugin.getSettings()))
				.setUserName((String) SqlNodes.USERNAME.getValue(plugin.getSettings()))
				.setPassword((String) SqlNodes.PASSWORD.getValue(plugin.getSettings()))
				.getConnection();
	}
	public void init() {
		//Moved this to new java thread (To Improve server TPS)
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if(		  Bukkit.getPluginManager()==null 
						||!Bukkit.getPluginManager().isPluginEnabled("EpicSet-Tokens")
						)
					return;
				long time = System.currentTimeMillis();
				onlineCache=connection.getOnlineData();
				if((boolean) SqlNodes.ENABLE_CACHE.getValue(plugin.getSettings()))
					offlineCache=connection.getOfflineData((int) SqlNodes.CACHE_SIZE.getValue(plugin.getSettings()));
				time=System.currentTimeMillis()-time;
				long size = 0;
				for(UUID id:offlineCache.keySet())
					size=size+16+((EpicTokenData) offlineCache.get(id)).getSize();
				if(time>60000) {
					plugin.getLogger().log(Level.SEVERE, "MySQL Server took too much time to request data ("+time+"ms).");
				}
				if(size>600000) {
					plugin.getLogger().log(Level.SEVERE, "Too much player's data is being cached right now ("+size+" bytes).");
					plugin.getLogger().log(Level.SEVERE, "Dropping Some Player's data to save space.");
					dropUselessCache();
				}
			}
		}.runTaskTimerAsynchronously(plugin, 20*5, 20*20);
		
	}
	/**
	 * Update the cache for the specific player
	 * If player didnt exist within the database then new entry is auto
	 * added
	 * @param player
	 */
	public void updateCache(final Player player) {
		new BukkitRunnable() {
			@Override
			public void run() {
				EpicTokenData data = connection.getPlayerData(player,true);
				if(data==null)
					return;
				plugin.injectMembers(data);
				TokenData oldData = null;
				if(onlineCache.containsKey(player.getUniqueId()))
					oldData = onlineCache.get(player.getUniqueId());
				else if(offlineCache.containsKey(player.getUniqueId()))
					oldData = offlineCache.get(player.getUniqueId());
				if(oldData!=null) {
					if(oldData.getTokens()!=data.getTokens()) {
						int oldTokens=oldData.getTokens();
						int newTokens=data.getTokens();
						TokensChangeEvent event = new TokensChangeEvent(data, newTokens-oldTokens, ChangeType.DATABASE);
						Bukkit.getPluginManager().callEvent(event);
					}
				}
				offlineCache.put(player.getUniqueId(), data);
				onlineCache.put(player.getUniqueId(), data);

				if(player.isOnline())
					offlineCache.remove(player.getUniqueId());
				else
					onlineCache.remove(player.getUniqueId());
			}
		}.runTaskAsynchronously(plugin);
	}
	/**
	 * Updates cache for a player name
	 * if the name doesnt exist in the database then no data is cached
	 * @param playerName
	 */
	public void updateCache(final UUID playerUuid) {
		new BukkitRunnable() {
			@Override
			public void run() {
				EpicTokenData data = connection.getPlayerData(playerUuid,true);
				if(data==null)
					return;
					plugin.injectMembers(data);
				TokenData oldData = null;
				if(onlineCache.containsKey(playerUuid))
					oldData = onlineCache.get(playerUuid);
				else if(offlineCache.containsKey(playerUuid))
					oldData = offlineCache.get(playerUuid);
				if(oldData!=null) {
					if(oldData.getTokens()!=data.getTokens()) {
						int oldTokens=oldData.getTokens();
						int newTokens=data.getTokens();
						TokensChangeEvent event = new TokensChangeEvent(data, newTokens-oldTokens, ChangeType.DATABASE);
						Bukkit.getPluginManager().callEvent(event);
					}
				}
				offlineCache.put(playerUuid, data);
				onlineCache.put(playerUuid, data);

				if(Bukkit.getOfflinePlayer(playerUuid).isOnline())
					offlineCache.remove(playerUuid);
				else
					onlineCache.remove(playerUuid);
			}
		}.runTaskAsynchronously(plugin);
	}
	public List<TokenData> getCached() {
		List<TokenData> data=new ArrayList<TokenData>();
		data.addAll(offlineCache.values());
		data.addAll(onlineCache.values());
		return data;
	}
	@Override
	/**
	 * Check if data for this player is already cached
	 * @param player
	 * @return
	 */
	public boolean isCached(Player player) {
		return this.offlineCache.containsKey(player.getUniqueId()) || this.onlineCache.containsKey(player.getUniqueId());
	}

	@Override
	public boolean isCached(UUID playerUuid) {
		return offlineCache.containsKey(playerUuid) || onlineCache.containsKey(playerUuid);
	}
	/**
	 * Set updates player's data to the database
	 * Throws IllegalStateException if attempted to use cached data
	 * since it can be De-synchronized from database.
	 * @param data
	 */
	public void setData(final TokenData data) {
		if(((EpicTokenData) data).isCached())
			throw new IllegalStateException("Cached Data cannot be set. Use getLatest() for un-cached version");
		if(Bukkit.getOfflinePlayer(data.getUuid()).isOnline()) {
			this.onlineCache.put(data.getUuid(), data);
			this.offlineCache.remove(data.getUuid());
		}
		else {
			this.offlineCache.put(data.getUuid(), data);
			this.onlineCache.remove(data.getUuid());
		}
		new BukkitRunnable() {
			@Override
			public void run() {
				connection.setPlayerData(data);
			}
		}.runTaskAsynchronously(plugin);
	}
	@Override
	/**
	 * Returns the last cached data for a player
	 * if it isnt cached then it returns null
	 * @param player
	 * @return
	 */
	public TokenData getCached(Player player) {
		if(onlineCache.containsKey(player.getUniqueId()))
			return onlineCache.get(player.getUniqueId());
		if(offlineCache.containsKey(player.getUniqueId()))
			return offlineCache.get(player.getUniqueId());
		return null;
	}
	@Override
	/**
	 * Returns the last cached data for playerName
	 * if it isnt cached then it returns null
	 * @param playerName
	 * @return
	 */
	public TokenData getCached(UUID playerUuid) {
		if(onlineCache.containsKey(playerUuid))
			return onlineCache.get(playerUuid);
		if(offlineCache.containsKey(playerUuid))
			return offlineCache.get(playerUuid);
		return null;
	}
	@Override
	/**
	 * Get the latest data for a player
	 * this method will return a completeablefuture so
	 * server doesnt lag until data is retrieved 
	 * @param player
	 * @return
	 */
	public CompletableFuture<TokenData> getLatest(OfflinePlayer player) {
		final CompletableFuture<TokenData> task=new CompletableFuture<TokenData>();
		new BukkitRunnable() {
			@Override
			public void run() {
				EpicTokenData data = connection.getPlayerData(player,false);
				if(data!=null)
					plugin.injectMembers(data);
				task.complete(data);
				if(Bukkit.getOfflinePlayer(data.getUuid()).isOnline()) {
					onlineCache.put(data.getUuid(), data);
					offlineCache.remove(data.getUuid());
				}
				else {
					offlineCache.put(data.getUuid(), data);
					onlineCache.remove(data.getUuid());
				}
			}
		}.runTaskAsynchronously(plugin);
		return task;
	}
	@Override
	/**
	 * Get the latest data for a playerName
	 * this method will return a completeablefuture so
	 * server doesnt lag until data is retrieved.
	 * Completed value will be null if name doesnt exist inside the
	 * database already
	 * @param player
	 * @return
	 */
	public CompletableFuture<TokenData> getLatest(UUID playerUuid) {
		final CompletableFuture<TokenData> task=new CompletableFuture<TokenData>();
		new BukkitRunnable() {
			@Override
			public void run() {
				EpicTokenData data = connection.getPlayerData(playerUuid,false);
				if(data!=null) {
					plugin.injectMembers(data);
					if(Bukkit.getOfflinePlayer(data.getUuid()).isOnline()) {
						onlineCache.put(data.getUuid(), data);
						offlineCache.remove(data.getUuid());
					}
					else {
						offlineCache.put(data.getUuid(), data);
						onlineCache.remove(data.getUuid());
					}
				}
				task.complete(data);
			}
		}.runTaskAsynchronously(plugin);
		return task;
	}
	@SuppressWarnings("deprecation")
	@Override
	public boolean isCached(String playerName) {
		OfflinePlayer player=Bukkit.getOfflinePlayer(playerName);
		if(player!=null)
			return isCached(player.getUniqueId());
		return false;
	}
	@SuppressWarnings("deprecation")
	@Override
	public TokenData getCached(String playerName) {
		OfflinePlayer player=Bukkit.getOfflinePlayer(playerName);
		if(player!=null)
			return getCached(player.getUniqueId());
		return null;
	}
	@Override
	public CompletableFuture<TokenData> getLatest(String playerName) {
		@SuppressWarnings("deprecation")
		OfflinePlayer player=Bukkit.getOfflinePlayer(playerName);
		if(player!=null)
			return getLatest(player.getUniqueId());
		return null;
	}
	
	/**
	 * Drop any useless cache currently stored
	 * This uses player's playtime to signify the importance
	 * Less play time is less important
	 */
	private void dropUselessCache() {
		int sizeToDrop=offlineCache.size()-500;
		for(int index=0;index<sizeToDrop;index++) {
			UUID toDrop=null;
			int playTime=Integer.MIN_VALUE;
			for(UUID id:offlineCache.keySet()) {
				if(Bukkit.getOfflinePlayer(id)==null) {
					toDrop=id;
					break;
				}
				else if(Bukkit.getOfflinePlayer(id)!=null && Bukkit.getOfflinePlayer(id).getStatistic(Statistic.PLAY_ONE_MINUTE)>playTime) {
					toDrop=id;
					playTime=Bukkit.getOfflinePlayer(id).getStatistic(Statistic.PLAY_ONE_MINUTE);
				}
			}
			if(toDrop!=null) {
				offlineCache.remove(toDrop);
			}
		}

		plugin.getLogger().log(Level.SEVERE, "Dropped cache for "+sizeToDrop+" Players.");
	}
}
