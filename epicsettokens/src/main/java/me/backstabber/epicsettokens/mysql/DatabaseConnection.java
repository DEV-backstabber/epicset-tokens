package me.backstabber.epicsettokens.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;


import me.backstabber.epicsettokens.DependencyInjector;
import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.data.TokenData;
import me.backstabber.epicsettokens.data.EpicTokenData;
import me.backstabber.epicsettokens.utils.ColorUtils;

public class DatabaseConnection extends DependencyInjector {
	public DatabaseConnection(EpicSetTokens plugin) {
		super(plugin);
	}
	private String host;
	private String port;
	private String database;
	private String username;
	private String password;
	private boolean isUseable=false;
	private Connection connection;
	public void create(String host,String port,String database,String username,String password) {
		this.host=host;
		this.port=port;
		this.database=database;
		this.username=username;
		this.password=password;
		ConsoleCommandSender console = Bukkit.getConsoleSender();
		new BukkitRunnable() {
			@Override
			public void run() {
				openConnection();
				if(!isUseable) {
					new BukkitRunnable() {
						@Override
						public void run() {
							console.sendMessage(ColorUtils.applyColor("&a[EpicSet-Tokens] >&cErrors Occoured when Connecting to the Database"));
							console.sendMessage(ColorUtils.applyColor("&a[EpicSet-Tokens] >&cPlugin will stop now."));
							console.sendMessage(ColorUtils.applyColor("&e-----------------------------------------------------------------------------------------"));
							new BukkitRunnable() {
								@Override
								public void run() {
									plugin.connected();
								}
							}.runTaskLater(plugin, 20);
							plugin.disconnected();
						}
					}.runTask(plugin);

					return;
				}
				console.sendMessage(ColorUtils.applyColor("&a[EpicSet-Tokens] >&aSucessfully Established Connection With the Sql-Database."));
				console.sendMessage(ColorUtils.applyColor("&a[EpicSet-Tokens] >&aPlugin will start now."));
				console.sendMessage(ColorUtils.applyColor("&e-----------------------------------------------------------------------------------------"));
				new BukkitRunnable() {
					@Override
					public void run() {
						plugin.connected();
					}
				}.runTaskLater(plugin, 20);
			}
		}.runTaskAsynchronously(plugin);
	}
	public EpicTokenData getPlayerData(OfflinePlayer player,boolean isCached) {
		EpicTokenData data=new EpicTokenData( plugin, player,isCached);
		if(!isUseable)
			return null;
		try {
			ResultSet result=connection.createStatement().executeQuery(
					"SELECT * from `epicTokensData` WHERE UUID = '"+player.getUniqueId()+"'"
			);
			if(result.next()) {
				data.updateTokens(result.getInt("Tokens"));
				return data;
			}
			else {
				setPlayerData(data);
				return data;
			}
		} catch(SQLException e) {
			
		}
		return null;
	}
	public Map<UUID, TokenData> getOfflineData(int maxSize) {
		Map<UUID, TokenData> result = new HashMap<UUID, TokenData>();
		if(!isUseable)
			return result;
		int index = 0;
		for(OfflinePlayer player:Bukkit.getOfflinePlayers()) {
			if(player.isOnline())
				continue;
			if(index>=maxSize)
				break;
			result.put(player.getUniqueId(), getPlayerData(player, true));
			index++;
		}
		return result;
	}
	public Map<UUID, TokenData> getOnlineData() {
		Map<UUID, TokenData> result = new HashMap<UUID, TokenData>();
		if(!isUseable)
			return result;
		for(OfflinePlayer player:Bukkit.getOnlinePlayers()) {
			result.put(player.getUniqueId(), getPlayerData(player, true));
		}
		return result;
	}
	public EpicTokenData getPlayerData(UUID uuid,boolean isCached) {
		EpicTokenData data=new EpicTokenData(plugin, uuid," ",isCached);
		if(!isUseable)
			return null;
		try {
			ResultSet result=connection.createStatement().executeQuery(
					"SELECT * from `epicTokensData` WHERE UUID = '"+uuid.toString()+"'"
			);
			if(result.next()) {
				data.updateTokens(result.getInt("Tokens"));
				data.setName(result.getString("Name"));
				return data;
			}
			else {
				setPlayerData(data);
				return data;
			}
		} catch(SQLException e) {
			
		}
		return null;
	}
	public boolean isSet(Player player) {
		try {
			ResultSet r = connection
					.createStatement()
					.executeQuery(
							"SELECT * FROM epicTokensData WHERE UUID = '" + 
									player.getUniqueId().toString() + "'");
			boolean contains = r.next();
			r.close();
			return contains;
	    } catch (SQLException e) {
	    	plugin.getLogger().log(Level.WARNING, "Could not check database for "+player.getName()+".");
	    	plugin.getLogger().log(Level.WARNING, e.getMessage());
	    	return false;
	    } 
	}
	public void setPlayerData(TokenData data) {
		if(!isUseable)
			return;
		try {
			if(data.getPlayerName()==null||data.getPlayerName().replace(" ", "").equals("")) {
				String statement= "INSERT into `epicTokensData` (UUID ,  Tokens ) Values ('"+data.getUuid().toString()+"' , "+data.getTokens()+")"
						+ " ON DUPLICATE KEY UPDATE Tokens = "+data.getTokens();
				connection.createStatement().execute(statement);
			}
			else {
				String statement= "INSERT into `epicTokensData` (UUID ,  Tokens ) Values ('"+data.getUuid().toString()+"' , "+data.getTokens()+")"
						+ " ON DUPLICATE KEY UPDATE Tokens = "+data.getTokens();
				connection.createStatement().execute(statement);
				statement= "INSERT into `epicTokensData` (UUID , Tokens , Name) Values ('"+data.getUuid().toString()+"' , "+data.getTokens()+" , "+"'"+data.getPlayerName()+"')"
						+ " ON DUPLICATE KEY UPDATE Name = '"+data.getPlayerName()+"'";
				connection.createStatement().execute(statement);
			}
		} catch (SQLException e) {
			plugin.getLogger().log(Level.WARNING, "Encountered error while updating data for "+data.getPlayerName()+".");
			plugin.getLogger().log(Level.WARNING, e.getMessage());
		}
	}
	public boolean isUseable() {
		return this.isUseable;
	}
	public void openConnection() {
	    try {
	    	connection = DriverManager.getConnection("jdbc:mysql://" + 
	          host + ":" + 
	          port + "/" + 
	          database, 
	          username, 
	          password);
	    	isUseable=true;
	    } catch (SQLException |NullPointerException e) {
			plugin.getLogger().log(Level.WARNING, "Failed to connect to database.(Did you configure it correctly?)");
			plugin.getLogger().log(Level.WARNING, e.getMessage());
			isUseable=false;
			return;
	    }
	    createTable();
	}
	public void closeConnection() {
		try {
	      if (!connection.isClosed() || connection != null)
	        connection.close(); 
	    } catch (SQLException e) {
			isUseable=false;
			plugin.getLogger().log(Level.WARNING, "Failed to connect to database.(Did you configure it correctly?)");
			plugin.getLogger().log(Level.WARNING, e.getMessage());
	    } 
	}
	private void createTable() {
		if(!isUseable)
			return;
		String statement="CREATE TABLE IF NOT EXISTS `epicTokensData` (\n"
				+ "`UUID` varchar(36) NOT NULL,\n"
				+ "`Tokens` int(11) unsigned NOT NULL,\n"
				+ "`Name` varchar(36),\n"
				+ "PRIMARY KEY  (`UUID`)\n)";
		try {
			this.connection.createStatement().execute(statement);
		} catch (SQLException e) {
			plugin.getLogger().log(Level.WARNING, "Unable to create table on Sql-database.");
			plugin.getLogger().log(Level.WARNING, e.getMessage());
		}
		statement = "DESC epicTokensData";
		try {
			ResultSet result = this.connection.createStatement().executeQuery(statement);
			while(result.next()) {
				if(result.getString("Field").equalsIgnoreCase("Name"))
					return;
			}
		} catch (SQLException e) {
			plugin.getLogger().log(Level.WARNING, "Unable to Check table Structure.");
			plugin.getLogger().log(Level.WARNING, e.getMessage());
		}
		plugin.getLogger().log(Level.INFO, "Found old Table Structure. Altering it.");
		statement = "ALTER TABLE mtokensdata\n" + 
				"    ADD Name varchar(36)";
		try {
			this.connection.createStatement().execute(statement);
		} catch (SQLException e) {
			plugin.getLogger().log(Level.WARNING, "Unable to Modify table on Sql-database.");
			plugin.getLogger().log(Level.WARNING, e.getMessage());
		}
	}
}
