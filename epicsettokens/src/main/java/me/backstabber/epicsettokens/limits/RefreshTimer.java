package me.backstabber.epicsettokens.limits;

import java.util.TimerTask;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.managers.ShopManager;
import me.backstabber.epicsettokens.shops.EpicShopManager;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;

public class RefreshTimer extends TimerTask{
	private EpicSetTokens plugin;
	private ShopManager shopManager;
	private RefreshType type;
	public RefreshTimer(RefreshType type, EpicSetTokens plugin, ShopManager shopManager) {
		this.plugin=plugin;
		this.shopManager=shopManager;
		this.type=type;
	}
	@Override
	public void run() {
		String message="&a[EpicSet-Tokens] &a[Limit-Resetter] Resetting Limits for &e"+CommonUtils.StringUtils.format(type.name())+".";
		Bukkit.getConsoleSender().sendMessage(ColorUtils.applyColor(message));
		ConfigurationSection node = plugin.getLimitSettings().getFile().getConfigurationSection("limit-refresher");
		if(node==null)
			return;
		for(String shopName:node.getKeys(false)) {
			if(shopManager.isShop(shopName)) {
				for(String index:node.getConfigurationSection(shopName).getKeys(false)) {
					String changeType=node.getString(shopName+"."+index);
					if(!changeType.equalsIgnoreCase(this.type.name()))
						continue;
					try {
					int i=Integer.valueOf(index);
					((EpicShopManager) shopManager).removeSlot(shopManager.getShop(shopName), i,false);
					}catch (Exception e) {
						plugin.getLogger().log(Level.INFO, "Unable to reset limits for shop "+shopName);
					}
					
				}
			}
		}
	}

}
