package me.backstabber.epicsettokens.limits;

public enum WeekDay {
	MONDAY(2),
	TUESDAY(3),
	WEDNESDAY(4),
	THURSDAY(5),
	FRIDAY(6),
	SATURDAY(7),
	SUNDAY(1);
	private int field;
	WeekDay(int field) {
		this.field=field;
	}
	public int getValue() {
		return this.field;
	}
}
