package me.backstabber.epicsettokens.limits;

public enum RefreshType {
	DAILY,
	WEEKLY,
	MONTHLY,
	YEARLY;
}

