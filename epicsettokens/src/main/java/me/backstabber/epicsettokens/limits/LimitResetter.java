package me.backstabber.epicsettokens.limits;

import java.util.Calendar;
import java.util.Timer;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;


import me.backstabber.epicsettokens.DependencyInjector;
import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;

public class LimitResetter extends DependencyInjector {
	public LimitResetter(EpicSetTokens plugin) {
		super(plugin);
	}

	@SuppressWarnings("deprecation")
	public void startRefreshTimer(RefreshType type) {
		ConfigurationSection node = plugin.getLimitSettings().getFile().getConfigurationSection("settings.refresher-settings."+type.name());
		//create timer & task
		Timer timer = new Timer();
		RefreshTimer task = new RefreshTimer(type, plugin,shopManager);
		plugin.injectMembers(task);
		//create a new calender
		Calendar today = Calendar.getInstance();
		today.set(Calendar.SECOND, node.getInt("sec"));
		today.set(Calendar.MINUTE, node.getInt("min"));
		today.set(Calendar.HOUR_OF_DAY, node.getInt("hour"));
		switch (type) {
		case DAILY:
			if(today.compareTo(Calendar.getInstance())<0) {
				today.add(Calendar.DAY_OF_MONTH, 1);
			}
			timer.schedule(task, today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
			break;
		case WEEKLY:
			String day=node.getString("weekday").toUpperCase();
			try {
				WeekDay week=WeekDay.valueOf(day);
				today.set(Calendar.DAY_OF_WEEK, week.getValue());
				if(today.compareTo(Calendar.getInstance())<0) {
					today.add(Calendar.DAY_OF_MONTH, 7);
				}
				timer.schedule(task, today.getTime(), TimeUnit.MILLISECONDS.convert(7, TimeUnit.DAYS));
			}catch (Exception e) {
				plugin.getLogger().log(Level.WARNING,"Unable to understand week "+day);
			}
			break;
		case MONTHLY:
			today.set(Calendar.DAY_OF_MONTH, node.getInt("day"));
			if(today.compareTo(Calendar.getInstance())<0) {
				today.add(Calendar.MONTH, 1);
			}
			timer.schedule(task, today.getTime(), TimeUnit.MILLISECONDS.convert(30, TimeUnit.DAYS));
			break;
		case YEARLY:
			today.set(Calendar.DAY_OF_MONTH, node.getInt("day"));
			today.set(Calendar.MONTH, node.getInt("month"));
			if(today.compareTo(Calendar.getInstance())<0) {
				today.add(Calendar.YEAR, 1);
			}
			timer.schedule(task, today.getTime(), TimeUnit.MILLISECONDS.convert(365, TimeUnit.DAYS));
			break;
		default:
			break;
		}
		String message="&a[EpicSet-Tokens] &a[Limit-Resetter] Started &e"+CommonUtils.StringUtils.format(type.name())+" &aresetter at time &e"+today.getTime().toLocaleString();
		Bukkit.getConsoleSender().sendMessage(ColorUtils.applyColor(message));
	}
}
