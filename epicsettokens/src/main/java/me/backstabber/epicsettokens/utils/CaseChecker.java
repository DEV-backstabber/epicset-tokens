package me.backstabber.epicsettokens.utils;

import java.util.List;


import me.backstabber.epicsettokens.DependencyInjector;
import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.data.TokenData;
import me.backstabber.epicsettokens.mysql.MySqlManager;

public class CaseChecker extends DependencyInjector {
	
	public CaseChecker(EpicSetTokens plugin) {
		super(plugin);
	}
	public TokenData getClosestUuid(String name) {
		List<TokenData> table=((MySqlManager)tokenManager).getCached();
		//simple check
		for(TokenData token:table) {
			if(token.getPlayerName().equalsIgnoreCase(name))
				return token;
		}
		//initials check
		for(TokenData token:table) {
			if(token.getPlayerName().toLowerCase().startsWith(name.toLowerCase()))
				return token;
		}
		//mixed check (5x)
		if(name.length()>1) {
			String chunk = name.substring(0,name.length()-1);
			for(int times=1;times<=5;times++) {
				for(TokenData token:table) {
					if(token.getPlayerName().toLowerCase().startsWith(chunk.toLowerCase()))
						return token;
				}
				if(chunk.length()>1)
					chunk = chunk.substring(0,chunk.length()-1);
				else
					return null;
			}
		}
		return null;
	}
}
