package me.backstabber.epicsettokens.shops.builder;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.builder.ShopBuilder;
import me.backstabber.epicsettokens.api.builder.ShopItemBuilder;
import me.backstabber.epicsettokens.api.permission.ShopPermission;
import me.backstabber.epicsettokens.api.shops.ShopPaths;
import me.backstabber.epicsettokens.api.shops.ShopTriggers;
import me.backstabber.epicsettokens.api.shops.TokenShop;
import me.backstabber.epicsettokens.permission.EpicShopPermission;
import me.backstabber.epicsettokens.shops.EpicTokenShop;
import me.backstabber.epicsettokens.utils.CommonUtils;
import me.backstabber.epicsettokens.utils.YamlManager;
/**
 * This is a simplified builder used to build new
 * & modify existing TokenShops
 * @author Backstabber
 *
 */
public class EpicShopBuilder implements ShopBuilder {
	private String name;
	private String displayName;
	private int rows=3;
	private ItemStack background;
	private int discountPer=0;
	private int discountTime=0;
	
	private Map<Integer, ShopItemBuilder> items=new HashMap<Integer, ShopItemBuilder>();
	
	private ShopPermission permission;
	public static ShopBuilder createNew(String name) {
		name=name.toLowerCase().replace(" ", "_");
		EpicSetTokens plugin = EpicSetTokens.getPlugin(EpicSetTokens.class);
		if(plugin.getShopManager().isShop(name)) 
			throw new IllegalArgumentException("Shop by name "+name+" already exists. Delete it first.");
		EpicShopBuilder builder = new EpicShopBuilder();
		builder.name=name;
		builder.setBackground(
				CommonUtils.BukkitUtils.getCustomItem(plugin.getSettings().getFile().getConfigurationSection("shop-settings.background"))
				);
		return builder;
	}
	public static ShopBuilder edit(String name) {
		name=name.toLowerCase().replace(" ", "_");
		EpicSetTokens plugin = EpicSetTokens.getPlugin(EpicSetTokens.class);
		if(!plugin.getShopManager().isShop(name)) 
			throw new IllegalArgumentException("No shop by name "+name+" exist. Create it first.");
		EpicShopBuilder builder = new EpicShopBuilder();
		builder.name=name;
		builder.setBackground(
				CommonUtils.BukkitUtils.getCustomItem(plugin.getSettings().getFile().getConfigurationSection("shop-settings.background"))
				);
		if(plugin.getShopManager().isShop(name)) {
			FileConfiguration shop = new YamlManager(plugin, name, "Shops").getFile();
			builder.displayName=shop.getString(ShopPaths.GUI_DISPLAY_NAME.getPath());
			builder.rows=shop.getInt(ShopPaths.GUI_SIZE.getPath());
			builder.background=CommonUtils.BukkitUtils.getItemFromNode(shop, ShopPaths.BACKGROUND_ITEM.getPath());
			builder.discountPer=shop.getInt(ShopPaths.DISCOUNT_PERCENTAGE.getPath());
			builder.discountTime=shop.getInt(ShopPaths.DISCOUNT_TIME.getPath());
			for(int i=0;i<9*builder.rows;i++) {
				if(shop.isSet(ShopPaths.ITEMS.getPath()+"."+i+".display")) {
					ItemStack displayItem=CommonUtils.BukkitUtils.getItemFromNode(shop, ShopPaths.ITEMS.getPath()+"."+i+".display");
					ShopItemBuilder item = EpicShopItemBuilder.create(displayItem)
							.setTrigger(ShopTriggers.valueOf(shop.getString(ShopPaths.ITEMS.getPath()+"."+i+".trigger")));
					if(ShopTriggers.valueOf(shop.getString(ShopPaths.ITEMS.getPath()+"."+i+".trigger")).equals(ShopTriggers.OPEN_SHOP))
						item.setClickShop(plugin.getShopManager().getShop(shop.getString(ShopPaths.ITEMS.getPath()+"."+i+".open-shop")));
					if(!ShopTriggers.valueOf(shop.getString(ShopPaths.ITEMS.getPath()+"."+i+".trigger")).equals(ShopTriggers.ITEM)) {
						builder.addItem(i, item);
						continue;
					}
					item.setPrice(shop.getInt(ShopPaths.ITEMS.getPath()+"."+i+".price"));
					item.setLimit(shop.getInt(ShopPaths.ITEMS.getPath()+"."+i+".limit"));
					item.giveDisplayItem(shop.getBoolean(ShopPaths.ITEMS.getPath()+"."+i+".give-display-item"));
					item.setCommands(shop.getStringList(ShopPaths.ITEMS.getPath()+"."+i+".purchase.commands"));
					builder.addItem(i, item);
					
				}
			}
		}
		return builder;
	}
	@Override
	public ShopBuilder setDisplayName(String displayName) {
		this.displayName=displayName;
		return this;
	}
	@Override
	public ShopBuilder setRows(int rows) {
		rows=Math.abs(rows);
		if(rows<0||rows>6)
			throw new IllegalArgumentException("Rows cant be greater than 6.");
		this.rows=rows;
		return this;
	}
	@Override
	public ShopBuilder setBackground(ItemStack background) {
		this.background=background.clone();
		return this;
	}
	@Override
	public ShopBuilder addDiscount(int percentage,int time) {
		this.discountPer=percentage;
		this.discountTime=time;
		return this;
	}
	@Override
	public ShopBuilder removeDiscount() {
		this.discountPer=0;
		this.discountTime=0;
		return this;
	}
	@Override
	public ShopBuilder addItem(int slot,ShopItemBuilder item) {
		this.items.put(slot, item);
		return this;
	}
	@Override
	public ShopBuilder removeItem(int slot) {
		this.items.remove(slot);
		return this;
	}
	@Override
	public ShopBuilder addPermission(ShopPermission permission) {
		this.permission=permission;
		return this;
	}
	@Override
	public TokenShop getShop() {
		EpicSetTokens plugin = EpicSetTokens.getPlugin(EpicSetTokens.class);
		YamlManager shop = new YamlManager(plugin, name,"Shops");
		FileConfiguration file=shop.getFile();
		file.set(ShopPaths.GUI_NAME.getPath(), name);
		file.set(ShopPaths.GUI_DISPLAY_NAME.getPath(), displayName);
		file.set(ShopPaths.GUI_SIZE.getPath(), rows);
		CommonUtils.BukkitUtils.saveItem(file, ShopPaths.BACKGROUND_ITEM.getPath(), background,false);
		file.set(ShopPaths.DISCOUNT_PERCENTAGE.getPath(), discountPer);
		file.set(ShopPaths.DISCOUNT_TIME.getPath(), discountTime);
		if(discountPer>0)
			file.set(ShopPaths.DISCOUNT_STAMP.getPath(), System.currentTimeMillis()/1000D);
		file.set(ShopPaths.ITEMS.getPath(), null);
		for(int slot:items.keySet())
			((EpicShopItemBuilder) items.get(slot)).apply(slot, file);
		if(this.permission!=null) {
			((EpicShopPermission)permission).save(file);
		}
		shop.save(false);
		EpicTokenShop tokenShop = new EpicTokenShop(plugin, name, shop);
		plugin.injectMembers(tokenShop);
		return tokenShop;
	}
}
