package me.backstabber.epicsettokens.shops.builder;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.builder.ShopItemBuilder;
import me.backstabber.epicsettokens.api.shops.ShopItem;
import me.backstabber.epicsettokens.api.shops.ShopPaths;
import me.backstabber.epicsettokens.api.shops.ShopTriggers;
import me.backstabber.epicsettokens.api.shops.TokenShop;
import me.backstabber.epicsettokens.shops.EpicShopItem;
import me.backstabber.epicsettokens.utils.CommonUtils;
/**
 * This is an item builder used to create an
 * item for the TokenShop builder.
 * @author Backstabber
 *
 */
public class EpicShopItemBuilder implements ShopItemBuilder {
	private ItemStack display;
	private ShopTriggers trigger=ShopTriggers.NONE;
	
	private int price;
	private int limit=-1;
	
	private boolean giveDisplay;
	private List<String> commands=new ArrayList<String>();
	
	private String shopName;
	public static ShopItemBuilder create(ItemStack display) {
		EpicShopItemBuilder builder = new EpicShopItemBuilder();
		builder.display=display;
		return builder;
	}
	@Override
	public ShopItemBuilder setTrigger(ShopTriggers trigger) {
		this.trigger=trigger;
		return this;
	}
	@Override
	public ShopItemBuilder setPrice(int price) {
		if(this.trigger!=null&&this.trigger.equals(ShopTriggers.ITEM))
			this.price=price;
		return this;
	}
	@Override
	public ShopItemBuilder setLimit(int limit) {
		if(limit<1)
			limit=-1;
		if(this.trigger!=null&&this.trigger.equals(ShopTriggers.ITEM))
			this.limit=limit;
		return this;
	}
	@Override
	public ShopItemBuilder giveDisplayItem(boolean enabled) {
		if(this.trigger!=null&&this.trigger.equals(ShopTriggers.ITEM))
			this.giveDisplay=enabled;
		return this;
	}
	@Override
	public ShopItemBuilder addCommand(String command) {
		if(this.trigger!=null&&this.trigger.equals(ShopTriggers.ITEM))
			this.commands.add(command);
		return this;
	}
	@Override
	public ShopItemBuilder setCommands(List<String> commands) {
		if(this.trigger!=null&&this.trigger.equals(ShopTriggers.ITEM))
			this.commands=commands;
		return this;
	}
	@Override
	public ShopItemBuilder setClickShop(TokenShop tokenShop) {
		if(this.trigger!=null&&this.trigger.equals(ShopTriggers.OPEN_SHOP))
			this.shopName=tokenShop.getName();
		return this;
	}
	@Override
	public ShopItem getItem(TokenShop shop,int slot) {
		FileConfiguration file=new YamlConfiguration();
		apply(slot, file);
		return new EpicShopItem(EpicSetTokens.getPlugin(EpicSetTokens.class), slot, file.getConfigurationSection(ShopPaths.ITEMS.getPath()+"."+slot), shop);
	}
	void apply(int slot,FileConfiguration file) {
		if(this.trigger.equals(ShopTriggers.ITEM))
			CommonUtils.BukkitUtils.saveItem(file, ShopPaths.ITEMS.getPath()+"."+slot+".display", display,true);
		else
			CommonUtils.BukkitUtils.saveItem(file, ShopPaths.ITEMS.getPath()+"."+slot+".display", display,false);
		file.set(ShopPaths.ITEMS.getPath()+"."+slot+".trigger", trigger.name());
		if(trigger.equals(ShopTriggers.OPEN_SHOP))
			file.set(ShopPaths.ITEMS.getPath()+"."+slot+".open-shop", shopName);
		if(!trigger.equals(ShopTriggers.ITEM))
			return;
		file.set(ShopPaths.ITEMS.getPath()+"."+slot+".price", price);
		file.set(ShopPaths.ITEMS.getPath()+"."+slot+".limit", limit);
		file.set(ShopPaths.ITEMS.getPath()+"."+slot+".purchase.give-display-item", giveDisplay);
		file.set(ShopPaths.ITEMS.getPath()+"."+slot+".purchase.commands", commands);
	}
}
