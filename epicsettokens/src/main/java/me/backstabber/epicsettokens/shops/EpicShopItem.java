package me.backstabber.epicsettokens.shops;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;


import me.backstabber.epicsettokens.DependencyInjector;
import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.data.TokenData;
import me.backstabber.epicsettokens.api.events.TokenShopAttemptPurchaseEvent;
import me.backstabber.epicsettokens.api.events.TokenShopAttemptPurchaseEvent.PurchaseState;
import me.backstabber.epicsettokens.api.events.TokensChangeEvent.ChangeType;
import me.backstabber.epicsettokens.api.shops.ShopItem;
import me.backstabber.epicsettokens.api.shops.ShopTriggers;
import me.backstabber.epicsettokens.api.shops.TokenShop;
import me.backstabber.epicsettokens.api.events.TokenShopPurchaseEvent;
import me.backstabber.epicsettokens.data.EpicPlayerData;
import me.backstabber.epicsettokens.data.EpicTokenData;
import me.backstabber.epicsettokens.mysql.MySqlManager;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;

public class EpicShopItem extends DependencyInjector implements ShopItem {
	private int slot;
	private ItemStack item;
	private ItemStack displayItem;
	private ShopTriggers trigger=ShopTriggers.NONE;
	private int price;
	private int limit=-1;
	
	private boolean giveDisplay;
	private List<String> commands=new ArrayList<String>();
	private String newShop;
	
	private TokenShop linkedShop;
	public EpicShopItem(EpicSetTokens plugin, int slot,ConfigurationSection configurationSection,TokenShop shop) {
		super(plugin);
		this.slot=slot;
		this.linkedShop=shop;
		this.item=CommonUtils.BukkitUtils.getItemFromNode(configurationSection, "display");
		this.trigger=ShopTriggers.valueOf(configurationSection.getString("trigger"));
		if(trigger.equals(ShopTriggers.OPEN_SHOP))
			this.newShop=configurationSection.getString("open-shop");
		if(trigger.equals(ShopTriggers.ITEM)) {
			this.price=configurationSection.getInt("price");
			this.limit=configurationSection.getInt("limit");
			this.giveDisplay=configurationSection.getBoolean("purchase.give-display-item");
			this.commands=configurationSection.getStringList("purchase.commands");
		}
	}
	@Override
	/**
	 * Get the slot in which this item is inside the gui
	 * @return slot number
	 */
	public int getSlot() {
		return this.slot;
	}
	@Override
	/**
	 * Set new limit for this item
	 * @param limit
	 */
	public void setLimit(int limit) {
		this.limit=limit;
	}
	@Override
	/**
	 * Get the limit of purchases of this item
	 *  0 or -1 means no limit
	 * @return limit
	 */
	public int getLimit() {
		return this.limit;
	}
	@Override
	/**
	 * Set the display item to a fixed itemstack
	 * @param itemstack to use as display (can have placeholders in name & lore)
	 */
	public void setDisplayItem(ItemStack display) {
		this.displayItem=display;
	}
	@Override
	/**
	 * Get the display item of this ShopItem
	 * Since it is displayed differently for each player & shop
	 * so player & linked shop are passed as parameters
	 * @param player
	 * @param EpicTokenShop instance
	 * @return ItemStack used as the display item
	 */
	public ItemStack getDisplayItem(Player player) {
		int tokens=0;
		if(tokenManager.getCached(player)!=null)
			tokens=tokenManager.getCached(player).getTokens();
		ItemStack display=(this.displayItem==null) ? item.clone():displayItem.clone();
		ItemMeta meta=display.getItemMeta();
		List<String> lore=new ArrayList<String>();
		if(item.hasItemMeta()&&item.getItemMeta().hasDisplayName()) {
			meta.setDisplayName(ColorUtils.applyColor(
					item.getItemMeta().getDisplayName()
					.replace("%player%", player.getName())
					.replace("%balance%", CommonUtils.NumericsUtils.formatTwoDecimals(tokens))
					));
		}
		if(display.hasItemMeta()&&display.getItemMeta().hasLore())
			for(String s:display.getItemMeta().getLore())
				lore.add(ColorUtils.applyColor(s
						.replace("%player%", player.getName())
						.replace("%balance%", CommonUtils.NumericsUtils.formatTwoDecimals(tokens))
						.replace("%totalpurchases%", CommonUtils.NumericsUtils.formatTwoDecimals(dataManager.getPlayerData(player).getPurchases(linkedShop, this)))
						));
		if(trigger.equals(ShopTriggers.ITEM)) {
			if(linkedShop.isDiscount()) {
				int discounted=linkedShop.getDiscountedPrice(price);
				for(String s:plugin.getSettings().getFile().getStringList("shop-settings.end-lore.sale"))
					lore.add(ColorUtils.applyColor(s
							.replace("%time%", CommonUtils.TemporalUtils.getFormattedTime(linkedShop.getDiscountTime(),plugin.getSettings().getFile().getConfigurationSection("time-settings")))
							.replace("%price%", CommonUtils.NumericsUtils.formatTwoDecimals(discounted))
							.replace("%totalpurchases%", CommonUtils.NumericsUtils.formatTwoDecimals(dataManager.getPlayerData(player).getPurchases(linkedShop, this)))
							.replace("%oldprice%", CommonUtils.NumericsUtils.formatTwoDecimals(price))));
			}
			else {
				for(String s:plugin.getSettings().getFile().getStringList("shop-settings.end-lore.price"))
					lore.add(ColorUtils.applyColor(s
							.replace("%totalpurchases%", CommonUtils.NumericsUtils.formatTwoDecimals(dataManager.getPlayerData(player).getPurchases(linkedShop, this)))
							.replace("%time%", CommonUtils.TemporalUtils.getFormattedTime(linkedShop.getDiscountTime(),plugin.getSettings().getFile().getConfigurationSection("time-settings")))
							.replace("%price%", CommonUtils.NumericsUtils.formatTwoDecimals(price))));
			}
			if(this.limit>0) {
				for(String s:plugin.getSettings().getFile().getStringList("shop-settings.end-lore.limit"))
					lore.add(ColorUtils.applyColor(s
							.replace("%totalpurchases%", CommonUtils.NumericsUtils.formatTwoDecimals(dataManager.getPlayerData(player).getPurchases(linkedShop, this)))
							.replace("%plimit%", CommonUtils.NumericsUtils.formatTwoDecimals(dataManager.getPlayerData(player).getLimitPurchases(linkedShop, this)))
							.replace("%limit%", CommonUtils.NumericsUtils.formatTwoDecimals(limit))));
			}
			for(String s:plugin.getSettings().getFile().getStringList("shop-settings.end-lore.footer"))
				lore.add(ColorUtils.applyColor(s
						.replace("%totalpurchases%", CommonUtils.NumericsUtils.formatTwoDecimals(dataManager.getPlayerData(player).getPurchases(linkedShop, this)))
						.replace("%time%", CommonUtils.TemporalUtils.getFormattedTime(linkedShop.getDiscountTime(),plugin.getSettings().getFile().getConfigurationSection("time-settings")))
						.replace("%oldprice%", CommonUtils.NumericsUtils.formatTwoDecimals(price))));
		}
		meta.setLore(lore);
		display.setItemMeta(meta);
		return display.clone();
		
	}
	/**
	 * used to handle any click on this gui button
	 * Player is the player who clicked
	 * Shop is the shop containing this item
	 * @param player
	 * @param shop
	 * @return if updating gui is required
	 */
	public boolean handleClick(Player player) {
		switch (this.trigger) {
		case OPEN_SHOP:
			shopManager.openShop(player, shopManager.getShop(newShop));
			return false;
		case CLOSE_GUI:
			shopManager.closeShop(player);
			return false;
		case ITEM:
			purchase(player);
			return true;
		case NONE:
		default:
			return false;
		}
	}
	@Override
	/**
	 * Processes a purchase request & sends any
	 * related messages regarding a purchase
	 * @param player
	 * @param shop
	 */
	public void purchase(Player player) {
		EpicPlayerData data=(EpicPlayerData) dataManager.getPlayerData(player);
		if(data.isDelayed(linkedShop, this)) {
			TokenShopAttemptPurchaseEvent event = new TokenShopAttemptPurchaseEvent(player, linkedShop, this, PurchaseState.DELAYED);
			Bukkit.getPluginManager().callEvent(event);
			if(!event.isCancelled()) {
				for(String s:plugin.getSettings().getFile().getStringList("messages.on-purchase-delayed"))
					player.sendMessage(ColorUtils.applyColor(s
							.replace("%limit%", CommonUtils.NumericsUtils.formatTwoDecimals(limit))
							.replace("%time%", CommonUtils.TemporalUtils.getFormattedTime(plugin.getSettings().getFile().getInt("shop-settings.purchase-delay"), plugin.getSettings().getFile().getConfigurationSection("time-settings")))
							.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))));
				return;
			}
		}
		if(!data.canPurchase(linkedShop, this)) {
			TokenShopAttemptPurchaseEvent event = new TokenShopAttemptPurchaseEvent(player, linkedShop, this, PurchaseState.LIMITED);
			Bukkit.getPluginManager().callEvent(event);
			if(!event.isCancelled()) {
				for(String s:plugin.getSettings().getFile().getStringList("messages.on-limit-reached"))
					player.sendMessage(ColorUtils.applyColor(s
							.replace("%limit%", CommonUtils.NumericsUtils.formatTwoDecimals(limit))
							.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))));
				return;
			}
		}
		int newPrice=linkedShop.getDiscountedPrice(price);
		if(tokenManager.getCached(player)==null) {
			TokenShopAttemptPurchaseEvent event = new TokenShopAttemptPurchaseEvent(player, linkedShop, this, PurchaseState.UNKNOWN);
			Bukkit.getPluginManager().callEvent(event);
			for(String s:plugin.getSettings().getFile().getStringList("messages.on-processing-fail"))
				player.sendMessage(ColorUtils.applyColor(s
						.replace("%limit%", CommonUtils.NumericsUtils.formatTwoDecimals(limit))
						.replace("%token%", CommonUtils.NumericsUtils.formatTwoDecimals(newPrice))
						.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))));
			((MySqlManager) tokenManager).updateCache(player);
			return;
		}
		if(tokenManager.getCached(player).getTokens()<newPrice) {
			TokenShopAttemptPurchaseEvent event = new TokenShopAttemptPurchaseEvent(player, linkedShop, this, PurchaseState.FAILED);
			Bukkit.getPluginManager().callEvent(event);
			if(!event.isCancelled()) {
				for(String s:plugin.getSettings().getFile().getStringList("messages.on-purchase-fail"))
					player.sendMessage(ColorUtils.applyColor(s
							.replace("%limit%", CommonUtils.NumericsUtils.formatTwoDecimals(limit))
							.replace("%token%", CommonUtils.NumericsUtils.formatTwoDecimals(newPrice))
							.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))));
				return;
			}
		}
		new BukkitRunnable() {
			@Override
			public void run() {
				CompletableFuture<TokenData> futureData = tokenManager.getLatest(player);
				while(!futureData.isDone())
					try { Thread.sleep(10);}catch (Exception e) {}
				TokenData latestData = futureData.getNow(null);
				if(latestData==null) {
					TokenShopAttemptPurchaseEvent event = new TokenShopAttemptPurchaseEvent(player, linkedShop, EpicShopItem.this, PurchaseState.UNKNOWN);
					Bukkit.getPluginManager().callEvent(event);
					for(String s:plugin.getSettings().getFile().getStringList("messages.on-processing-fail"))
						player.sendMessage(ColorUtils.applyColor(s
								.replace("%limit%", CommonUtils.NumericsUtils.formatTwoDecimals(limit))
								.replace("%token%", CommonUtils.NumericsUtils.formatTwoDecimals(newPrice))
								.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))));
					return;
				}
				if(latestData.getTokens()<newPrice) {
					TokenShopAttemptPurchaseEvent event = new TokenShopAttemptPurchaseEvent(player, linkedShop, EpicShopItem.this, PurchaseState.FAILED);
					Bukkit.getPluginManager().callEvent(event);
					if(!event.isCancelled()) {
						for(String s:plugin.getSettings().getFile().getStringList("messages.on-purchase-fail"))
							player.sendMessage(ColorUtils.applyColor(s
									.replace("%limit%", CommonUtils.NumericsUtils.formatTwoDecimals(limit))
									.replace("%token%", CommonUtils.NumericsUtils.formatTwoDecimals(newPrice))
									.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))));
						return;
					}
				}
				try {
					((EpicTokenData) latestData).setTokens(latestData.getTokens()-newPrice,ChangeType.PURCHASE);
				}catch (IllegalStateException e) {
					TokenShopAttemptPurchaseEvent event = new TokenShopAttemptPurchaseEvent(player, linkedShop, EpicShopItem.this, PurchaseState.UNKNOWN);
					Bukkit.getPluginManager().callEvent(event);
					for(String s:plugin.getSettings().getFile().getStringList("messages.on-processing-fail"))
						player.sendMessage(ColorUtils.applyColor(s
								.replace("%limit%", CommonUtils.NumericsUtils.formatTwoDecimals(limit))
								.replace("%token%", CommonUtils.NumericsUtils.formatTwoDecimals(newPrice))
								.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))));
					return;
				}
				new BukkitRunnable() {
					@Override
					public void run() {
						TokenShopAttemptPurchaseEvent event = new TokenShopAttemptPurchaseEvent(player, linkedShop, EpicShopItem.this, PurchaseState.SUCESS);
						Bukkit.getPluginManager().callEvent(event);
						if(!event.isCancelled())
							completePurchase(player);
					}
				}.runTask(plugin);
			}
		}.runTaskAsynchronously(plugin);
		for(String s:plugin.getSettings().getFile().getStringList("messages.on-purchase-processing"))
			player.sendMessage(ColorUtils.applyColor(s
					.replace("%limit%", CommonUtils.NumericsUtils.formatTwoDecimals(limit))
					.replace("%token%", CommonUtils.NumericsUtils.formatTwoDecimals(newPrice))
					.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))));
		return;
	}
	/**
	 * Completes a purchase by a player 
	 * This includes sucess messages & item / command execution
	 * @param player
	 * @param shop
	 */
	private void completePurchase(Player player) {
		((EpicPlayerData) dataManager.getPlayerData(player)).addPurchase(linkedShop, this);
		TokenShopPurchaseEvent event = new TokenShopPurchaseEvent(player, linkedShop, this);
		Bukkit.getPluginManager().callEvent(event);
		if(event.isCancelled())
			return;
		int newPrice=linkedShop.getDiscountedPrice(price);
		for(String s:plugin.getSettings().getFile().getStringList("messages.on-purchase-sucess"))
			player.sendMessage(ColorUtils.applyColor(s
					.replace("%limit%", CommonUtils.NumericsUtils.formatTwoDecimals(limit))
					.replace("%token%", CommonUtils.NumericsUtils.formatTwoDecimals(newPrice))
					.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))));
		for(String s:plugin.getSettings().getFile().getStringList("messages.on-purchase-broadcast"))
			Bukkit.broadcastMessage(ColorUtils.applyColor(s
					.replace("%limit%", CommonUtils.NumericsUtils.formatTwoDecimals(limit))
					.replace("%player%", player.getName())
					.replace("%token%", CommonUtils.NumericsUtils.formatTwoDecimals(newPrice))
					.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))));
		if(giveDisplay)
			CommonUtils.BukkitUtils.giveItem(player, item.clone());
		for(String cmd:commands) {
			if(cmd.startsWith("<ccmd>"))
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.substring(6)
						.replace("%player%", player.getName())
						.replace("%token%", newPrice+"")
						.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))
						);
			else if(cmd.startsWith("<pcmd>"))
				Bukkit.dispatchCommand(player, cmd.substring(6)
						.replace("%player%", player.getName())
						.replace("%token%", newPrice+"")
						.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))
						);
			else if(cmd.startsWith("<pmsg>"))
				player.sendMessage(ColorUtils.applyColor(cmd.substring(6))
						.replace("%player%", player.getName())
						.replace("%token%", CommonUtils.NumericsUtils.formatTwoDecimals(newPrice))
						.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))
						);
			else
				Bukkit.broadcastMessage(cmd
						.replace("%player%", player.getName())
						.replace("%token%", CommonUtils.NumericsUtils.formatTwoDecimals(newPrice))
						.replace("%item%", CommonUtils.BukkitUtils.getItemName(item))
						);
		}
		if(!event.getShop().getName().equals(linkedShop.getName())) {
			shopManager.openShop(player, event.getShop());
		}
		else {
			((EpicTokenShop) linkedShop).openGui(player);
		}
	}
}
