package me.backstabber.epicsettokens.shops;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


import me.backstabber.epicsettokens.DependencyInjector;
import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.events.TokenShopCloseEvent;
import me.backstabber.epicsettokens.api.events.TokenShopOpenEvent;
import me.backstabber.epicsettokens.api.events.TokenShopSwapEvent;
import me.backstabber.epicsettokens.api.managers.ShopManager;
import me.backstabber.epicsettokens.api.shops.TokenShop;
import me.backstabber.epicsettokens.utils.CommonUtils;
import me.backstabber.epicsettokens.utils.YamlManager;

public class EpicShopManager extends DependencyInjector implements ShopManager {
	public EpicShopManager(EpicSetTokens plugin) {
		super(plugin);
	}
	private Map<Player, EpicTokenShop> openedShops=new HashMap<Player, EpicTokenShop>();
	@Override
	/**
	 * Get TokenShop of the name defined
	 * Use isShop(name) to check if shop exists
	 * @param shop name
	 * @return TokenShop if it exists
	 */
	public TokenShop getShop(String name) {
		if(isShop(name)) {
			EpicTokenShop shop = new EpicTokenShop(plugin, name, new YamlManager(plugin, name,"Shops"));
			plugin.injectMembers(shop);
			return shop;
		}
		return null;
	}
	@Override
	/**
	 * Returns names of all acknowledged shops
	 * @return List of all shops
	 */
	public List<String> getAllShops() {
		List<String> shops=new ArrayList<String>();
		for(String name:getAllFiles("Shops")) {
			if(isShop(name))
				shops.add(name);
		}
		if(!shops.contains("main")) {
			new YamlManager(plugin, "main","Shops");
			shops.add("main");
		}
		return shops;
	}
	@Override
	/**
	 * Checks if a shop is registered for a name
	 * @param name of shop
	 * @return if it exists
	 */
	public boolean isShop(String name) {
		File file=new File(plugin.getDataFolder()+"/Shops",name+".yml");
		if(file.exists()) {
			if(! CommonUtils.BukkitUtils.checkYmlFile(file)) {
				file.delete();
				return false;
			}
			return true;
		}
		return file.exists();
	}
	@Override
	/**
	 * Checks if a player has a shop open
	 * @param player to check
	 * @return if its open
	 */
	public boolean isShopOpen(Player player) {
		if(openedShops.containsKey(player)) {
			if(openedShops.get(player).getViewer()!=null&&openedShops.get(player).getViewer().equals(player))
				return true;
			openedShops.remove(player);
			return false;
		}
		return false;
	}
	@Override
	/**
	 * Opens a shop for a player
	 * @param player
	 * @param shop
	 */
	public void openShop(Player player , TokenShop shop) {
		TokenShop shopInstance=getShop(shop.getName());
		if(!((EpicTokenShop)shopInstance).checkPermission(player))
			return;
		if(!openedShops.containsKey(player)) {
			TokenShopOpenEvent openEvent = new TokenShopOpenEvent(player, shopInstance);
			Bukkit.getPluginManager().callEvent(openEvent);
			if(openEvent.isCancelled())
				return;
			((EpicTokenShop) openEvent.getShop()).openGui(player);
			this.openedShops.put(player, (EpicTokenShop) openEvent.getShop());
		}
		else
		{
			TokenShopSwapEvent swapEvent=new TokenShopSwapEvent(player, openedShops.get(player), shopInstance);
			Bukkit.getPluginManager().callEvent(swapEvent);
			if(swapEvent.isCancelled())
				return;
			((EpicTokenShop) swapEvent.getNewShop()).openGui(player);
			this.openedShops.put(player, (EpicTokenShop) swapEvent.getNewShop());
		}
	}
	@Override
	/**
	 * Gets the shop currently open for the player
	 * @param player
	 * @return TokenShop instance
	 */
	public TokenShop getOpenShop(Player player) {
		if(isShopOpen(player))
			return openedShops.get(player);
		return null;
	}
	@Override
	/**
	 * Delete a tokenshop permanently
	 * @param shop
	 */
	public void deleteShop(TokenShop shop) {
		for(Player player:openedShops.keySet())
			if(openedShops.get(player).getName().equalsIgnoreCase(shop.getName()))
				player.closeInventory();
		removeSlot(shop);
		File file = new File(plugin.getDataFolder()+"/Shops",shop.getName()+".yml");
		file.delete();
	}
	@Override
	/**
	 * Close shop for a player
	 * @param player
	 */
	public void closeShop(Player player) {
		if(openedShops.containsKey(player)) {
			TokenShopCloseEvent closeEvent = new TokenShopCloseEvent(player, openedShops.get(player));
			Bukkit.getPluginManager().callEvent(closeEvent);
			if(closeEvent.isCancelled())
				return;
			player.closeInventory();
			openedShops.remove(player);
		}
	}
	public void removeSlot(TokenShop shop,int slot,boolean removeCounts) {
		dataManager.removeSlot(shop, slot,removeCounts);
	}
	public void removeSlot(TokenShop shop) {
		dataManager.removeSlot(shop);
		
	}
	private List<String> getAllFiles(String path) {
		File folder = new File(plugin.getDataFolder() + "/" + path);
		String[] fileNames = folder.list();
		ArrayList<String> names = new ArrayList<String>();
		if (fileNames != null) {
			for (String s : fileNames) {
				names.add(s.replace(".yml", ""));
			}
		}
		return names;
	}
}
