package me.backstabber.epicsettokens.shops;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


import me.backstabber.epicsettokens.DependencyInjector;
import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.api.permission.ShopPermission;
import me.backstabber.epicsettokens.api.shops.ShopPaths;
import me.backstabber.epicsettokens.api.shops.TokenShop;
import me.backstabber.epicsettokens.permission.EpicShopPermission;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;
import me.backstabber.epicsettokens.utils.YamlManager;


public class EpicTokenShop extends DependencyInjector implements TokenShop {
	
	private YamlManager manager;
	private Player viewer;
	private String name;
	private Inventory gui;
	private List<EpicShopItem> items=new ArrayList<EpicShopItem>();
	
	private ShopPermission permission;
	public EpicTokenShop(EpicSetTokens plugin, String name,YamlManager manager) {
		super(plugin);
		this.name=name;
		this.manager=manager;
		if(manager.getFile().isSet(ShopPaths.PERMISSION.getPath())) {
			String node = manager.getFile().getString(ShopPaths.PERMISSION.getPath());
			List<String> message = manager.getFile().getStringList(ShopPaths.PERMISSION_MESSAGE.getPath());
			this.permission=new EpicShopPermission(node, message);
		}
	}
	
	public YamlManager getParent() {
		return manager;
	}
	@Override
	public String getName() {
		return name;
	}
	@Override
	public boolean isGuiOpen() {
		return (viewer != null);
	}
	@Override
	public boolean hasPermission(Player player) {
		return (this.permission!=null) ? permission.hasPermission(player) : true;
	}
	@Override
	public Player getViewer() {
		return viewer;
	}

	public void handleClick(InventoryClickEvent clickEvent) {
		if (clickEvent.getRawSlot() < 0)
			return;
		Inventory inv = clickEvent.getClickedInventory();
		Player player = (Player) clickEvent.getWhoClicked();
		if (viewer != null && player.equals(viewer) && inv.equals(gui)) {
			clickEvent.setCancelled(true);
			int slot=clickEvent.getRawSlot();
			for(EpicShopItem item:items)
				if(item.getSlot()==slot) {
					item.handleClick(player);
					break;
				}
		}
	}

	public void handleClose(InventoryCloseEvent closeEvent) {
		if (this.viewer == null)
			return;
		Player player = (Player) closeEvent.getPlayer();
		Inventory inv = closeEvent.getInventory();
		if (player.equals(viewer) && inv.equals(gui))
			this.viewer = null;
	}
	
	@Override
	public boolean isDiscount() {
		int per=manager.getFile().getInt(ShopPaths.DISCOUNT_PERCENTAGE.getPath());
		double startTime=manager.getFile().getDouble(ShopPaths.DISCOUNT_STAMP.getPath());
		int timeSecs=manager.getFile().getInt(ShopPaths.DISCOUNT_TIME.getPath());
		double currentTime=System.currentTimeMillis()/1000D;
		if(per>0&&((startTime+timeSecs)>currentTime)) {
			return true;
		}
		else {
			manager.getFile().set(ShopPaths.DISCOUNT_PERCENTAGE.getPath(), 0);
			manager.getFile().set(ShopPaths.DISCOUNT_STAMP.getPath(), 0);
			manager.getFile().set(ShopPaths.DISCOUNT_TIME.getPath(), 0);
			manager.save(false);
			return false;
		}
	}
	@Override
	public int getDiscountedPrice(int originalPrice) {
		if(isDiscount()) {
			int per=manager.getFile().getInt(ShopPaths.DISCOUNT_PERCENTAGE.getPath());
			if(per<=100) {
			int off=(int) (((double)originalPrice*(double)per)/100D);
			int result=originalPrice-off;
			return result;
			}
		}
		return originalPrice;
	}
	@Override
	public int getDiscountTime() {
		if(isDiscount()) {
			double startTime=manager.getFile().getDouble(ShopPaths.DISCOUNT_STAMP.getPath());
			int timeSecs=manager.getFile().getInt(ShopPaths.DISCOUNT_TIME.getPath());
			double currentTime=System.currentTimeMillis()/1000D;
			return (int) (timeSecs-(currentTime-startTime));
		}
		return 0;
	}
	void openGui(Player player) {
		if(!hasPermission(player))
			return;
		if (viewer != null&&!viewer.equals(player)) {

			viewer.closeInventory();
			viewer = null;
		}
		viewer = player;
		player.openInventory(createGui(player));
	}
	boolean checkPermission(Player player) {
		return (this.permission!=null) ? permission.checkPermission(player) : true;
	}
	private Inventory createGui(Player player) {
		this.items.clear();
		this.items=new ArrayList<EpicShopItem>();
		this.gui=Bukkit.createInventory(null
				, 9*manager.getFile().getInt(ShopPaths.GUI_SIZE.getPath())
				, ColorUtils.applyColor(manager.getFile().getString(ShopPaths.GUI_DISPLAY_NAME.getPath()))
				);
		ItemStack background=CommonUtils.BukkitUtils.getItemFromNode(manager.getFile(), ShopPaths.BACKGROUND_ITEM.getPath());
		FileConfiguration file = manager.getFile();
		for(int i=0;i<9*manager.getFile().getInt(ShopPaths.GUI_SIZE.getPath());i++) {
			if(file.getConfigurationSection(ShopPaths.ITEMS.getPath()+"."+i)!=null) {
				EpicShopItem item=new EpicShopItem(plugin, i, file.getConfigurationSection(ShopPaths.ITEMS.getPath()+"."+i),this);
				plugin.injectMembers(item);
				gui.setItem(i, item.getDisplayItem(player).clone());
				this.items.add(item);
			}
			else
				gui.setItem(i, background);
		}
		return this.gui;
	}
}
