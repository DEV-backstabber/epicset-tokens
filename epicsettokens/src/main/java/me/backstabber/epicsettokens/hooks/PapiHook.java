package me.backstabber.epicsettokens.hooks;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;


import me.backstabber.epicsettokens.EpicSetTokens;
import me.backstabber.epicsettokens.mysql.MySqlManager;
import me.backstabber.epicsettokens.utils.ColorUtils;
import me.backstabber.epicsettokens.utils.CommonUtils;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;

public class PapiHook extends PlaceholderExpansion {
	private EpicSetTokens plugin;
	private MySqlManager sqlManager;
	
	public PapiHook(EpicSetTokens plugin,MySqlManager manager) {
		this.plugin=plugin;
		this.sqlManager=manager;
	}
	public void init() 
	{
		ConsoleCommandSender console = Bukkit.getConsoleSender();
		console.sendMessage(ColorUtils.applyColor("&a[EpicSet-Tokens] >&eFound PlaceholderAPI. &rRegistering placehlders."));
		this.register();
	}
	@Override
	public String onPlaceholderRequest(Player player, String identifier) {
		try {
			if(identifier.equals("tokens")) {
				if(!sqlManager.isCached(player)) {
					sqlManager.updateCache(player);
					return "0";
				}
				return CommonUtils.NumericsUtils.formatTwoDecimals(sqlManager.getCached(player).getTokens());
			}
	
			if(identifier.equals("tokens_formatted")) {
				if(!sqlManager.isCached(player)) {
					sqlManager.updateCache(player);
					return "0";
				}
				return CommonUtils.NumericsUtils.formatPrefixedNumbers(sqlManager.getCached(player).getTokens());
			}
	
			if(identifier.equals("tokens_raw")) {
				if(!sqlManager.isCached(player)) {
					sqlManager.updateCache(player);
					return "0";
				}
				return sqlManager.getCached(player).getTokens()+"";
			}
			
			return "undefined";
		}
		catch(NullPointerException e) {
			return "0";
		}
		
	}
	@Override
	public boolean canRegister() {
		return true;
	}
	@Override
	public String getAuthor() {
		return plugin.getDescription().getAuthors().toString();
	}
	@Override
	public String getIdentifier() {
		return "epicset-tokens";
	}
	@Override
	public String getPlugin() {
		return plugin.getName();
	}
	@Override
	public String getVersion() {
		return plugin.getDescription().getVersion();
	}
}
